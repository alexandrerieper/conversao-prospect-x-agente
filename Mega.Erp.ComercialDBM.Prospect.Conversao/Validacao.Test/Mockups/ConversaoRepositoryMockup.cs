﻿using Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Models;

namespace Validacao.Test.Mockups
{
    class ConversaoRepositoryMockup : IConversaoRepository
    {
        public void BeginTransaction()
        {
            
        }

        public void Commit()
        {
            
        }

        public bool Delete(Prospect prospect)
        {
            throw new NotImplementedException();
        }

        public bool Edit(Prospect prospect)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Prospect> Get()
        {
            throw new NotImplementedException();
        }

        public Prospect Get(int id)
        {
            throw new NotImplementedException();
        }

        public bool Insert(Prospect prospect)
        {
            throw new NotImplementedException();
        }

        public void Rollback()
        {
            throw new NotImplementedException();
        }
    }
}
