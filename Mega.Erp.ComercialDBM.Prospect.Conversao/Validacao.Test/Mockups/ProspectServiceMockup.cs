﻿using Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Models;

namespace Validacao.Test.Mockups
{
    class ProspectServiceMockup : IProspectService
    {
        public bool Edit(Prospect prospect)
        {
            return true;
        }

        public IEnumerable<Prospect> Get()
        {
            throw new NotImplementedException();
        }

        public Prospect Get(decimal idProspect)
        {
            switch (idProspect) {
                case 0: return null;
                case 1: return new Prospect();
                case 2: return new Prospect()
                    {
                        TipoPessoa = "F",
                        Id = (int)idProspect
                    };
                case 3:
                    return new Prospect()
                    {
                        TipoPessoa = "F",
                        Id = (int)idProspect
                    };
                case 4:
                    return new Prospect()
                    {
                        TipoPessoa = "F",
                        Id = (int)idProspect,
                        CargoProspect = new Cargo()
                        {
                            Nome = "Costureira"
                        },
                        CargoCje = new Cargo(),
                        EstadoCivil = "C",
                    };
                case 5:
                    return new Prospect()
                    {
                        TipoPessoa = "F",
                        Id = (int)idProspect,
                        AgnIn = 1,
                        Nome = "ERRO",
                        Cpf = "112358",
                        CargoProspect = new Cargo()
                        {
                            Nome = "Costureira"
                        },
                        CargoCje = new Cargo()
                    };
                case 6:
                    return new Prospect()
                    {
                        TipoPessoa = "J",
                        Id = (int)idProspect,
                        AgnIn = 1,
                        Nome = "ERRO",
                        Cnpj = "112358",
                        CargoProspect = new Cargo()
                        {
                            Nome = "Costureira"
                        },
                        EstadoCivil = "C",
                        CargoCje = new Cargo()
                    };
                case 7:
                    return new Prospect()
                    {
                        TipoPessoa = "F",
                        Id = (int)idProspect,
                        AgnIn = 1,
                        Nome = "SUCESSO",
                        Cpf = "112358",
                        CargoProspect = new Cargo()
                        {
                            Nome = "Costureira"
                        },
                        EstadoCivil = "C",
                        CargoCje = new Cargo()
                    };
                case 8:
                    return new Prospect()
                    {
                        TipoPessoa = "F",
                        Id = (int)idProspect,
                        AgnIn = 1,
                        Nome = "SUCESSO",
                        Cpf = "112358",
                        CargoProspect = new Cargo()
                        {
                            Nome = "Costureira"
                        },
                        EstadoCivil = "C",
                        CargoCje = new Cargo()
                    };
                default: return null;
            }

            
        }

        public IEnumerable<EnderecoProspect> GetEnderecos(decimal idProspect)
        {
            List<EnderecoProspect> enderecos = new List<EnderecoProspect>();
            if ((idProspect == 2) || (idProspect == 3))
                return enderecos;

            enderecos.Add(new EnderecoProspect()
            {
                Id = 1,
                IdProspect = (int)idProspect,
            });
            enderecos.Add(new EnderecoProspect()
            {
                Id = 2,
                IdProspect = (int)idProspect,
            });
            enderecos.Add(new EnderecoProspect()
            {
                Id = 3,
                IdProspect = (int)idProspect,
            });
            enderecos.Add(new EnderecoProspect()
            {
                Id = 4,
                IdProspect = (int)idProspect,
            });
            enderecos.Add(new EnderecoProspect()
            {
                Id = 5,
                IdProspect = (int)idProspect,
            });

            return enderecos;
        }

        public IEnumerable<EnderecoProspect> GetEnderecosConjuge(decimal idProspect)
        {
            List<EnderecoProspect> enderecos = new List<EnderecoProspect>();
            if (idProspect == 2)
                return enderecos;

            enderecos.Add(new EnderecoProspect()
            {
                Id = 1,
                IdProspect = (int)idProspect,
            });
            if (idProspect == 3)
                return enderecos;

            enderecos.Add(new EnderecoProspect()
            {
                Id = 2,
                IdProspect = (int)idProspect,
            });
            enderecos.Add(new EnderecoProspect()
            {
                Id = 3,
                IdProspect = (int)idProspect,
            });
            enderecos.Add(new EnderecoProspect()
            {
                Id = 4,
                IdProspect = (int)idProspect,
            });
            enderecos.Add(new EnderecoProspect()
            {
                Id = 5,
                IdProspect = (int)idProspect,
            });

            return enderecos;
        }

        public IEnumerable<TelefoneProspect> GetTelefones(decimal idProspect)
        {
            List<TelefoneProspect> enderecos = new List<TelefoneProspect>();
            enderecos.Add(new TelefoneProspect()
            {
                Id = 1,
                IdProspect = (int)idProspect,
                Numero = "555-4689"
            });
            enderecos.Add(new TelefoneProspect()
            {
                Id = 2,
                IdProspect = (int)idProspect,
                Numero = "555-4689"
            });
            enderecos.Add(new TelefoneProspect()
            {
                Id = 3,
                IdProspect = (int)idProspect,
                Numero = "555-4689"
            });
            enderecos.Add(new TelefoneProspect()
            {
                Id = 4,
                IdProspect = (int)idProspect,
                Numero = "555-4689"
            });
            enderecos.Add(new TelefoneProspect()
            {
                Id = 5,
                IdProspect = (int)idProspect,
                Numero = "555-4689"
            });

            return enderecos;
        }
    }
}
