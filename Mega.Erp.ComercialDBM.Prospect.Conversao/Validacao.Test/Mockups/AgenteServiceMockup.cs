﻿using Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Models;

namespace Validacao.Test.Mockups
{
    class AgenteServiceMockup : IAgenteService
    {
        public bool Delete(Agente agente)
        {
            return true;
        }

        public bool Delete(decimal idAgente)
        {
            return true;
        }

        public bool DeleteAllEnderecos(Agente agente)
        {
            return true;
        }

        public bool Edit(Agente agente)
        {
            return true;
        }

        public bool Edit(PessoaFisica pessoaFisica)
        {
            return true;
        }

        public AgenteId GetAgenteId(Agente agente)
        {
            return null;
        }

        public Agente GetById(decimal id)
        {
            switch (id) {
                case 0: return null;
                case 1: return new Agente()
                    {
                        Tab = 1,
                        Pad = 1,
                        Nome = "ERRO",
                        CGC = "112358",
                        TipoPessoa = "F"
                    };
                case 2:
                    return new Agente()
                    {
                        TipoPessoa = "F"
                    };
                default: return null;
            };
        }

        public Agente GetByNomeCNPJ(string name, string cnpj)
        {
            if (name == "A" && cnpj == "1")
                return new Agente()
                {
                    Nome = name,
                    CGC = cnpj
                };
            else
                return null;
        }

        public Agente GetByNomeCPF(string name, string cpf)
        {
            if (name == "A" && cpf == "1")
                return new Agente()
                {
                    Nome = name,
                    CGC = cpf
                };
            else
                return null;
        }

        public decimal getNewIdAgente()
        {
            return 2;
        }

        public bool Insert(Agente agente)
        {
            return true;
        }

        public bool InsertCliente(Cliente agente)
        {
            return true;
        }

        public bool InsertCRM(AgenteCRM agenteCRM)
        {
            return true;
        }

        public bool InsertEndereco(EndAgente endAgente)
        {
            return true;
        }

        public bool InsertId(AgenteId agenteId)
        {
            return true;
        }

        public bool InsertPessoaFisica(PessoaFisica pessoaFisica)
        {
            return true;
        }
    }
}
