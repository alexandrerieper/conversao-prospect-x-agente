﻿using Xunit;
using FluentAssertions;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Core;

namespace Validacao.Test
{
    public class CPFsUnit
    {
      
        [Theory]
        [InlineData("861.408.794-26" , true)]
        [InlineData("544d65d4d4d564d", false)]
        [InlineData("000.000.000-00" , true)]
        [InlineData("269.368.375-06" , true)]
        [InlineData("123.456.456-98" , false)]
        [InlineData(null, false)]
        public void TesteValidaCPF(string cpf, bool valorEsperado)
        {
            bool cpfValido = Validacoes.IsCPFValido(cpf);
            cpfValido.ShouldBeEquivalentTo(valorEsperado);
        }        

    }
}
