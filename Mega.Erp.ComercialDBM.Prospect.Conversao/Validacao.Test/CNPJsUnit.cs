﻿using Xunit;
using FluentAssertions;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Core;

namespace Validacao.Test
{
    public class CNPJsUnit
    {
      
        [Theory]
        [InlineData("14.523.347/0001-23", true)]
        [InlineData("00.111.111/111-11" , false)] //invalido
        [InlineData("84.376.542/0001-53", true)]
        [InlineData("11.891.653/0001-24", true)]
        [InlineData("12.891.652/0001-22", false)] //invalido
        [InlineData("96.272.061/0001-87", true)]
        [InlineData(null, false)]
        public void TesteValidaCNPJ(string cnpj, bool valorEsperado)
        {
            bool cnpjValido = Validacoes.IsCNPJValido(cnpj);
            cnpjValido.ShouldBeEquivalentTo(valorEsperado);
        }        

    }
}
