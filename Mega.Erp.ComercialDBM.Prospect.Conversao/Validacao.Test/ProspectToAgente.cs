﻿using FluentAssertions;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validacao.Test.Mockups;
using Xunit;

namespace Validacao.Test
{
    public class ProspectToAgente
    {
        private AgenteServiceMockup _agenteService;
        private ProspectServiceMockup _prospectService;
        private ConversaoRepositoryMockup _repository;
        private ConversaoService _conversao;

        public ProspectToAgente()
        {
            _agenteService = new AgenteServiceMockup();
            _prospectService = new ProspectServiceMockup();
            _repository = new ConversaoRepositoryMockup();
            _conversao = new ConversaoService(_repository,_prospectService,_agenteService);
        }

        [Theory]
        [InlineData(0,0, true, 1)]
        [InlineData(1, 0, true, 6)]
        [InlineData(2, 0, true, 4)]
        [InlineData(3, 0, true, 5)]
        [InlineData(5, 0, true, 7)]
        [InlineData(6, 0, true, 8)]
        [InlineData(4, 0, true, 0)] //Insere novo agente
        [InlineData(7, 0, true, 0)] //atualiza agente
        [InlineData(8, 0, false, 0)] //não atualiza agente
        public void ConvertProspectToAgente(decimal idProspect, decimal padOrg, bool sincroniza, decimal valorEsperado)
        {
            decimal ret = _conversao.ProspectToAgente(idProspect, padOrg, sincroniza);
            ret.ShouldBeEquivalentTo(valorEsperado);
        }
    }
}
