﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Data.Common;
using Oracle.ManagedDataAccess.Client;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Services;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Repository;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces.Services;

namespace Mega.Erp.ComercialDBM.Prospect.Conversao
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
            services.AddScoped<ConversaoService>();
            services.AddScoped<IConversaoRepository, ConversaoRepository>();
            //Adição do serviços extenos
            services.AddScoped<IProspectService, ProspectService>();
            services.AddScoped<IProspectRepository, ProspectRepository>();
            services.AddScoped<IAgenteService, AgenteService>();
            services.AddScoped<IAgenteRepository, AgenteRepository>();
            services.AddScoped<DbConnection>(_ => {
                var connect = new OracleConnection("user id=mega;password=megamega;data source=192.130.130.3:1521/tmp1");
                connect.Open();
                return connect;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseMvc();
        }
    }
}
