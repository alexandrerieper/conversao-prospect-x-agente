﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Models;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces
{
    public interface IConversaoRepository
    {
        IEnumerable<Models.Prospect> Get();
        bool Insert(Models.Prospect prospect);
        bool Delete(Models.Prospect prospect);
        bool Edit(Models.Prospect prospect);
        Models.Prospect Get(int id);
        void BeginTransaction();
        void Rollback();
        void Commit();
    }
}
