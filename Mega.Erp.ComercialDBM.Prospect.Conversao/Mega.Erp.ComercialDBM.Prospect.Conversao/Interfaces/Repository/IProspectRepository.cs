﻿using Mega.Erp.ComercialDBM.Prospects.Conversao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces
{
    public interface IProspectRepository
    {
        IEnumerable<Models.Prospect> Get();
        Models.Prospect Get(decimal idProspect);
        bool Insert(Models.Prospect insert);
        bool Edit(Models.Prospect prospect);
        string GetNomeCargo(decimal idCargo);
        IEnumerable<TelefoneProspect> GetTelefones(decimal idProspect);
        IEnumerable<EnderecoProspect> GetEnderecos(decimal idProspect);
        IEnumerable<EnderecoProspect> GetEnderecosConjuge(decimal idProspect);
    }
}
