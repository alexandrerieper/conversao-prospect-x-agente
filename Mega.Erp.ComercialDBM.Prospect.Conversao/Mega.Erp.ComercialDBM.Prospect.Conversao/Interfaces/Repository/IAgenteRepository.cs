﻿using Mega.Erp.ComercialDBM.Prospects.Conversao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces
{
    public interface IAgenteRepository
    {
        Agente GetById(decimal id);
        Agente GetByNomeCNPJ(string name, string cnpj);
        Agente GetByNomeCPF(string name, string cpf);
        AgenteId getAgenteId(Agente agente);
        decimal getNewIdAgente();
        bool Insert(Agente agente);
        bool Insert(EndAgente endAgente);
        bool InsertCRM(AgenteCRM agenteCRM);
        bool InsertId(AgenteId agenteId);
        bool InsertPessoaFisica(PessoaFisica pessoaFisica);
        bool InsertParametroFiscal(ParametroFiscal parametroFiscal);
        bool InsertTelefone(TelefoneAgente telefoneAgente);
        bool InsertPessoa(PessoaAgente pessoaAgente);
        bool InsertCliente(Cliente cliente);
        bool InsertConta(ContaAgente contaAgente);
        bool InsertCampoEspecifico(AgenteCampoEspecifico campoEspecifico);
        bool EditCampoEspecifico(AgenteCampoEspecifico campoEspecifico);
        bool Edit(Agente agente);
        bool Edit(PessoaFisica pessoaFisica);
        bool Delete(Agente agente);
        bool Delete(decimal agente);
        bool DeleteConjuge(Agente agente);
        bool DeleteAllTelefone(Agente agente);
        bool DeleteAllEndereco(Agente agente);
    }
}
