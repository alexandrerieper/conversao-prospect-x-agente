﻿using Mega.Erp.ComercialDBM.Prospects.Conversao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces.Services
{
    public interface IProspectService
    {
        IEnumerable<Models.Prospect> Get();
        Models.Prospect Get(decimal idProspect);
        IEnumerable<TelefoneProspect> GetTelefones(decimal idProspect);
        IEnumerable<EnderecoProspect> GetEnderecos(decimal idProspect);
        IEnumerable<EnderecoProspect> GetEnderecosConjuge(decimal idProspect);
        bool Edit(Models.Prospect prospect);
    }
}
