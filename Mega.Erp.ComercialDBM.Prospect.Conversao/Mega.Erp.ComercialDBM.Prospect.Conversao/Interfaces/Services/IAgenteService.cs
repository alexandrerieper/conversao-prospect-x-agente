﻿using Mega.Erp.ComercialDBM.Prospects.Conversao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces.Services
{
    public interface IAgenteService
    {
        Agente GetById(decimal id);
        Agente GetByNomeCNPJ(string name, string cnpj);
        Agente GetByNomeCPF(string name, string cpf);
        AgenteId GetAgenteId(Agente agente);
        decimal getNewIdAgente();
        bool Edit(Agente agente);
        bool Edit(PessoaFisica pessoaFisica);
        bool Delete(Agente agente);
        bool Delete(decimal idAgente);
        bool DeleteAllEnderecos(Agente agente);
        bool Insert(Agente agente);
        bool InsertEndereco(EndAgente endAgente);
        bool InsertCRM(AgenteCRM agenteCRM);
        bool InsertId(AgenteId agenteId);
        bool InsertPessoaFisica(PessoaFisica pessoaFisica);
        bool InsertCliente(Cliente agente);
    }
}
