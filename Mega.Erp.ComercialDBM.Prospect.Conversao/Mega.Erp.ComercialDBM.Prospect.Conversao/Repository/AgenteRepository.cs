﻿using Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Models;
using System.Data.Common;
using Dapper;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Repository
{
    public class AgenteRepository : IAgenteRepository
    {
        private DbConnection _connection;

        public AgenteRepository(DbConnection connection)
        {
            _connection = connection;
        }

        public bool Delete(Agente agente)
        {
            return Delete(agente.Id);
        }

        public bool Delete(decimal agente)
        {
            _connection.Execute(@"DELETE FROM mgglo.glo_agentes
                                        WHERE agn_in_codigo = :agente"
                                , new { agente });
            return true;
        }

        public bool DeleteAllEndereco(Agente agente)
        {
            _connection.Execute(@"DELETE FROM mgglo.glo_endagentes
                                        WHERE agn_in_codigo = :agente"
                                , new { agente  = agente.Id });
            return true;

        }

        public bool Edit(Agente agente)
        {
            _connection.Execute(@"UPDATE mgglo.glo_agentes age
                      SET age.agn_st_fantasia     = :NomeFantasia  -- PM 34602
                        , age.agn_st_nome         = :Nome
                        , age.agn_ch_tipopessoafj = :TipoPessoa
                        , age.uf_st_sigla         = :EstadoSigla
                        , age.pa_st_sigla         = :EnderecoPaisSigla
                        , age.mun_in_codigo       = :IdMunicipio
                        , age.agn_st_municipio    = :EnderecoMunicipioId
                        , age.tpl_st_sigla        = :EnderecoLogradouroSigla
                        , age.agn_st_logradouro   = :EnderecoLogradouroNome
                        , age.agn_st_numero       = :EnderecoNumeroPredio
                        , age.agn_st_bairro       = :EnderecoNomeBairro
                        , age.agn_st_cep          = :EnderecoCEP
                        , age.agn_st_complemento  = :EnderecoComplemento
                        , age.agn_st_cgc          = :CGC
                        , age.agn_ch_statuscgc    = :CGCStatus  -- AWR 02/05/2012
                        , age.agn_st_email        = :Email
                        , age.agn_st_url          = :Url
                        , age.agn_st_cei           = :CEI
                        , age.agn_st_inscrestadual = :InscricaoEstadual
                        , age.agn_st_inscrmunic    = :InscricaoMunicipal

                    WHERE age.agn_in_codigo       = :Id",
                    new
                    { NomeFantasia = agente.NomeFantasia
                    , Nome = agente.Nome
                    , TipoPessoa = agente.TipoPessoa
                    , EstadoSigla = agente.EstadoSigla
                    , EnderecoPaisSigla = agente.EnderecoPaisSigla
                    , IdMunicipio = agente.EnderecoMunicipioId
                    , EnderecoMunicipioId = agente.EnderecoMunicipioId
                    , EnderecoLogradouroSigla = agente.EnderecoLogradouroSigla
                    , EnderecoLogradouroNome = agente.EnderecoLogradouroNome
                    , EnderecoNumeroPredio = agente.EnderecoNumeroPredio
                    , EnderecoNomeBairro = agente.EnderecoNomeBairro
                    , EnderecoCEP = agente.EnderecoCEP
                    , EnderecoComplemento = agente.EnderecoComplemento
                    , CGC = agente.CGC
                    , CGCStatus = agente.CGCStatus
                    , Email = agente.Email
                    , Url = agente.Url
                    , CEI = agente.CEI
                    , InscricaoEstadual = agente.InscricaoEstadual
                    , InscricaoMunicipal = agente.InscricaoMunicipal
                    , Id = agente.Id
                    });
            return true;
        }

        public bool InsertCampoEspecifico(AgenteCampoEspecifico campoEspecifico)
        {
            _connection.Execute(@"INSERT INTO mgglo.glo_agentecampoespecifico (agn_tab_in_codigo,
                                                      agn_pad_in_codigo,
                                                      agn_in_codigo,
                                                      agn_st_pacto_nupcial,
                                                      agn_dt_pacto_nupcial,
                                                      agn_dt_casamento,
                                                      agn_st_tabelionato,
                                                      agn_st_registro,
                                                      agn_st_livro,
                                                      agn_st_folhas)
                                              VALUES (:agn_tab_in_codigo,
                                                      :agn_pad_in_codigo,
                                                      :agn_in_codigo,
                                                      :agn_st_pacto_nupcial,
                                                      :agn_dt_pacto_nupcial,
                                                      :agn_dt_casamento,
                                                      :agn_st_tabelionato,
                                                      :agn_st_registro,
                                                      :agn_st_livro,
                                                      :agn_st_folhas);"
                                    ,new
                                    {
                                      AGN_TAB_IN_CODIGO = campoEspecifico.AgenteTab
                                    , AGN_PAD_IN_CODIGO = campoEspecifico.AgentePad
                                    , AGN_IN_CODIGO = campoEspecifico.AgenteId
                                    , AGN_ST_PACTO_NUPCIAL = campoEspecifico.PactoNupcial
                                    , AGN_DT_PACTO_NUPCIAL = campoEspecifico.DataPacto
                                    , AGN_DT_CASAMENTO = campoEspecifico.DataCasamento
                                    , AGN_ST_TABELIONATO = campoEspecifico.Tabelionato
                                    , AGN_ST_REGISTRO = campoEspecifico.Registro
                                    , AGN_ST_LIVRO = campoEspecifico.Livro
                                    , AGN_ST_FOLHAS = campoEspecifico.Folhas
                                    });
            return true;
        }

        public Agente GetById(decimal id)
        {
            return _connection.Query<Agente>(@"SELECT AGN_TAB_IN_CODIGO as Tab
                                                    , AGN_PAD_IN_CODIGO as Pad
                                                    , AGN_IN_CODIGO as Id
                                                    , AGN_BO_CONSOLIDADOR as TipoOrganizacao
                                                    , AGN_ST_FANTASIA as NomeFantasia
                                                    , AGN_ST_NOME as Nome
                                                    , AGN_CH_TIPOPESSOAFJ as TipoPessoa
                                                    , AGN_IN_NATJURID as NaturezaJuridica
                                                    , AGN_ST_INSCRMUNIC as InscricaoMunicipal
                                                    , UF_ST_SIGLA as EstadoSigla
                                                    , PA_ST_SIGLA as EnderecoPaisSigla
                                                    , MUN_IN_CODIGO as EnderecoMunicipioId
                                                    , AGN_ST_MUNICIPIO as EnderecoMunicipioNome
                                                    , TPL_ST_SIGLA as EnderecoLogradouroSigla
                                                    , AGN_ST_LOGRADOURO as EnderecoLogradouroNome
                                                    , AGN_ST_NUMERO as EnderecoNumeroPredio
                                                    , AGN_ST_BAIRRO as EnderecoNomeBairro
                                                    , AGN_ST_CEP as EnderecoCEP
                                                    , AGN_ST_CXPOSTAL as EnderecoCaixaPostal
                                                    , AGN_ST_COMPLEMENTO as EnderecoComplemento
                                                    , AGN_ST_CEPCXPOSTAL as EnderecoCEPCaixaPostal
                                                    , AGN_ST_REFERENCIA as EnderecoReferencia
                                                    , AGN_ST_ENQUADRAIPI as EnquadraIPI
                                                    , AGN_ST_ENQUADRAICMS as EnquadraICMS
                                                    , AGN_ST_ENQUADRAISS as EnquadraISS
                                                    , AGN_ST_CGC as CGC
                                                    , AGN_CH_STATUSCGC as CGCStatus
                                                    , AGN_ST_FRETEPESOVALOR as FretePesoValor
                                                    , AGN_ST_INSCRESTADUAL as InscricaoEstadual
                                                    , PAI_AGN_TAB_IN_CODIGO as TabPai
                                                    , AGN_ST_ATIVECONOMICA as AtividadeEconomica
                                                    , PAI_AGN_PAD_IN_CODIGO as PadPai
                                                    , PAI_AGN_IN_CODIGO as IdPai
                                                    , AGN_ST_SUFRAMA as Suframa
                                                    , AGN_ST_REPARTICAO as Reparticao
                                                    , AGN_ST_EMAIL as Email
                                                    , AGN_BO_IPISIMPLES as IPISimples
                                                    , AGN_BO_ICMSSIMPLES as ICMSSimples
                                                    , AGN_BO_ISSSIMPLES as ISSSimples
                                                    , AGN_BO_SIMPLES as OpcaoSimples
                                                    , FORP_IN_CODIGO as FormaPagamentoId
                                                    , AGN_BO_FLUXO as Fluxo
                                                    , AGN_IN_NIVEL as Nivel
                                                    , AGN_ST_ORDEM as Ordem
                                                    , AGN_DT_ULTIMAATUCAD as UltimaAtualizacao
                                                    , AGN_IN_VALIDADECAD as ValidadeCadastroEmDias
                                                    , AGN_ST_CEI as CEI
                                                    , AGN_CH_TIPOINSCRICAO as TipoInscricao
                                                    , AGN_CH_ENQUADRAMENTO as Enquadramento
                                                    , CNAE_ST_CODIGO as CDAEId
                                                    , AGN_BO_RETERIR as ReterIR
                                                    , AGN_BO_RETERINSS as ReterINSS
                                                    , AGN_ST_URL as Url
                                                    , AGN_ST_APELIDO as Apelido
                                                    , AGN_ST_SENHA as Senha
                                                    , AGN_CH_STATUSINSCRESTADUAL as StatusInscricaoEstadual
                                                    , AGN_BO_PUBLICO as Publico
                                                    , AGN_ST_INSCRPRODUTOR as InscricaoProdutoRural
                                                    , USU_IN_INCLUSAO as Insclusao
                                                    , AGN_CH_RURALTIPOPESSOAFJ as TipoPessoaRural
                                                    , TAB05_IN_CODIGO as ESocialId
                                                    , AGN_ST_NRCAEPF as ESocialCAEPF
                                                    , AGN_ST_NRCNO as ESocialCNO
                                                    , AGN_CH_TIPODESPADUANEIRA as TipoDespesaAduaneira
                                                    , AGN_IN_TIPOCAEPF as TipoAtividadeEconomicaPF
                                                    , AGN_CH_NATRETPISCOFINS as NaturezaRetencaoPIS_CONFINS
                                                    , AGN_BO_RATEIOAFRMM as PermiteRateioAFRMM
                                                    , AGN_CH_TIPOAFRMM as TipoRateioAFRMM
                                                    , AGN_ST_PISMEI as PISMEI
                                                    , AGN_ST_CBOMEI as CBOMEI
                                                    , AGN_ST_NIF as NIF
                                                    , DFP_IN_CODIGO as FontePagadoraId
                                                    , AGN_BO_DISPENSADONIF as DispensadoNIF
                                                 FROM mgglo.glo_agentes
                                                WHERE AGN_IN_CODIGO = :id"
                                             , new { id }).SingleOrDefault();
        }

        public Agente GetByNomeCNPJ(string name, string cnpj)
        {
            string nameUpper = name.ToUpper();
            string cnpjUpper = cnpj.ToUpper();
            return _connection.Query<Agente>(@"SELECT AGN.AGN_TAB_IN_CODIGO as Tab
                                                    , AGN.AGN_PAD_IN_CODIGO as Pad
                                                    , AGN.AGN_IN_CODIGO as Id
                                                    , AGN.AGN_BO_CONSOLIDADOR as TipoOrganizacao
                                                    , AGN.AGN_ST_FANTASIA as NomeFantasia
                                                    , AGN.AGN_ST_NOME as Nome
                                                    , AGN.AGN_CH_TIPOPESSOAFJ as TipoPessoa
                                                    , AGN.AGN_IN_NATJURID as NaturezaJuridica
                                                    , AGN.AGN_ST_INSCRMUNIC as InscricaoMunicipal
                                                    , AGN.UF_ST_SIGLA as EstadoSigla
                                                    , AGN.PA_ST_SIGLA as EnderecoPaisSigla
                                                    , AGN.MUN_IN_CODIGO as EnderecoMunicipioId
                                                    , AGN.AGN_ST_MUNICIPIO as EnderecoMunicipioNome
                                                    , AGN.TPL_ST_SIGLA as EnderecoLogradouroSigla
                                                    , AGN.AGN_ST_LOGRADOURO as EnderecoLogradouroNome
                                                    , AGN.AGN_ST_NUMERO as EnderecoNumeroPredio
                                                    , AGN.AGN_ST_BAIRRO as EnderecoNomeBairro
                                                    , AGN.AGN_ST_CEP as EnderecoCEP
                                                    , AGN.AGN_ST_CXPOSTAL as EnderecoCaixaPostal
                                                    , AGN.AGN_ST_COMPLEMENTO as EnderecoComplemento
                                                    , AGN.AGN_ST_CEPCXPOSTAL as EnderecoCEPCaixaPostal
                                                    , AGN.AGN_ST_REFERENCIA as EnderecoReferencia
                                                    , AGN.AGN_ST_ENQUADRAIPI as EnquadraIPI
                                                    , AGN.AGN_ST_ENQUADRAICMS as EnquadraICMS
                                                    , AGN.AGN_ST_ENQUADRAISS as EnquadraISS
                                                    , AGN.AGN_ST_CGC as CGC
                                                    , AGN.AGN_CH_STATUSCGC as CGCStatus
                                                    , AGN.AGN_ST_FRETEPESOVALOR as FretePesoValor
                                                    , AGN.AGN_ST_INSCRESTADUAL as InscricaoEstadual
                                                    , AGN.PAI_AGN_TAB_IN_CODIGO as TabPai
                                                    , AGN.AGN_ST_ATIVECONOMICA as AtividadeEconomica
                                                    , AGN.PAI_AGN_PAD_IN_CODIGO as PadPai
                                                    , AGN.PAI_AGN_IN_CODIGO as IdPai
                                                    , AGN.AGN_ST_SUFRAMA as Suframa
                                                    , AGN.AGN_ST_REPARTICAO as Reparticao
                                                    , AGN.AGN_ST_EMAIL as Email
                                                    , AGN.AGN_BO_IPISIMPLES as IPISimples
                                                    , AGN.AGN_BO_ICMSSIMPLES as ICMSSimples
                                                    , AGN.AGN_BO_ISSSIMPLES as ISSSimples
                                                    , AGN.AGN_BO_SIMPLES as OpcaoSimples
                                                    , AGN.FORP_IN_CODIGO as FormaPagamentoId
                                                    , AGN.AGN_BO_FLUXO as Fluxo
                                                    , AGN.AGN_IN_NIVEL as Nivel
                                                    , AGN.AGN_ST_ORDEM as Ordem
                                                    , AGN.AGN_DT_ULTIMAATUCAD as UltimaAtualizacao
                                                    , AGN.AGN_IN_VALIDADECAD as ValidadeCadastroEmDias
                                                    , AGN.AGN_ST_CEI as CEI
                                                    , AGN.AGN_CH_TIPOINSCRICAO as TipoInscricao
                                                    , AGN.AGN_CH_ENQUADRAMENTO as Enquadramento
                                                    , AGN.CNAE_ST_CODIGO as CDAEId
                                                    , AGN.AGN_BO_RETERIR as ReterIR
                                                    , AGN.AGN_BO_RETERINSS as ReterINSS
                                                    , AGN.AGN_ST_URL as Url
                                                    , AGN.AGN_ST_APELIDO as Apelido
                                                    , AGN.AGN_ST_SENHA as Senha
                                                    , AGN.AGN_CH_STATUSINSCRESTADUAL as StatusInscricaoEstadual
                                                    , AGN.AGN_BO_PUBLICO as Publico
                                                    , AGN.AGN_ST_INSCRPRODUTOR as InscricaoProdutoRural
                                                    , AGN.USU_IN_INCLUSAO as Insclusao
                                                    , AGN.AGN_CH_RURALTIPOPESSOAFJ as TipoPessoaRural
                                                    , AGN.TAB05_IN_CODIGO as ESocialId
                                                    , AGN.AGN_ST_NRCAEPF as ESocialCAEPF
                                                    , AGN.AGN_ST_NRCNO as ESocialCNO
                                                    , AGN.AGN_CH_TIPODESPADUANEIRA as TipoDespesaAduaneira
                                                    , AGN.AGN_IN_TIPOCAEPF as TipoAtividadeEconomicaPF
                                                    , AGN.AGN_CH_NATRETPISCOFINS as NaturezaRetencaoPIS_CONFINS
                                                    , AGN.AGN_BO_RATEIOAFRMM as PermiteRateioAFRMM
                                                    , AGN.AGN_CH_TIPOAFRMM as TipoRateioAFRMM
                                                    , AGN.AGN_ST_PISMEI as PISMEI
                                                    , AGN.AGN_ST_CBOMEI as CBOMEI
                                                    , AGN.AGN_ST_NIF as NIF
                                                    , AGN.DFP_IN_CODIGO as FontePagadoraId
                                                    , AGN.AGN_BO_DISPENSADONIF as DispensadoNIF
                                                    , fis.agn_st_cpf
                                                 FROM mgglo.glo_agentes agn
                                                 INNER JOIN mgglo.glo_pessoa_fisica fis on agn.agn_tab_in_codigo = fis.agn_tab_in_codigo
                                                                                       and agn.agn_pad_in_codigo = fis.agn_pad_in_codigo
                                                                                       and agn.agn_in_codigo     = fis.agn_in_codigo
                                                WHERE FIS.AGN_CH_TIPO       = 'P'
                                                AND UPPER(agn.agn_st_nome)  = :nameUpper
                                                AND UPPER(fis.agn_st_cpf)   = :cnpjUpper"
                                             , new { nameUpper, cnpjUpper}).SingleOrDefault();
        }

        public Agente GetByNomeCPF(string name, string cpf)
        {
            string nameUpper = name.ToUpper();
            return _connection.Query<Agente>(@"SELECT AGN_TAB_IN_CODIGO as Tab
                                                    , AGN_PAD_IN_CODIGO as Pad
                                                    , AGN_IN_CODIGO as Id
                                                    , AGN_BO_CONSOLIDADOR as TipoOrganizacao
                                                    , AGN_ST_FANTASIA as NomeFantasia
                                                    , AGN_ST_NOME as Nome
                                                    , AGN_CH_TIPOPESSOAFJ as TipoPessoa
                                                    , AGN_IN_NATJURID as NaturezaJuridica
                                                    , AGN_ST_INSCRMUNIC as InscricaoMunicipal
                                                    , UF_ST_SIGLA as EstadoSigla
                                                    , PA_ST_SIGLA as EnderecoPaisSigla
                                                    , MUN_IN_CODIGO as EnderecoMunicipioId
                                                    , AGN_ST_MUNICIPIO as EnderecoMunicipioNome
                                                    , TPL_ST_SIGLA as EnderecoLogradouroSigla
                                                    , AGN_ST_LOGRADOURO as EnderecoLogradouroNome
                                                    , AGN_ST_NUMERO as EnderecoNumeroPredio
                                                    , AGN_ST_BAIRRO as EnderecoNomeBairro
                                                    , AGN_ST_CEP as EnderecoCEP
                                                    , AGN_ST_CXPOSTAL as EnderecoCaixaPostal
                                                    , AGN_ST_COMPLEMENTO as EnderecoComplemento
                                                    , AGN_ST_CEPCXPOSTAL as EnderecoCEPCaixaPostal
                                                    , AGN_ST_REFERENCIA as EnderecoReferencia
                                                    , AGN_ST_ENQUADRAIPI as EnquadraIPI
                                                    , AGN_ST_ENQUADRAICMS as EnquadraICMS
                                                    , AGN_ST_ENQUADRAISS as EnquadraISS
                                                    , AGN_ST_CGC as CGC
                                                    , AGN_CH_STATUSCGC as CGCStatus
                                                    , AGN_ST_FRETEPESOVALOR as FretePesoValor
                                                    , AGN_ST_INSCRESTADUAL as InscricaoEstadual
                                                    , PAI_AGN_TAB_IN_CODIGO as TabPai
                                                    , AGN_ST_ATIVECONOMICA as AtividadeEconomica
                                                    , PAI_AGN_PAD_IN_CODIGO as PadPai
                                                    , PAI_AGN_IN_CODIGO as IdPai
                                                    , AGN_ST_SUFRAMA as Suframa
                                                    , AGN_ST_REPARTICAO as Reparticao
                                                    , AGN_ST_EMAIL as Email
                                                    , AGN_BO_IPISIMPLES as IPISimples
                                                    , AGN_BO_ICMSSIMPLES as ICMSSimples
                                                    , AGN_BO_ISSSIMPLES as ISSSimples
                                                    , AGN_BO_SIMPLES as OpcaoSimples
                                                    , FORP_IN_CODIGO as FormaPagamentoId
                                                    , AGN_BO_FLUXO as Fluxo
                                                    , AGN_IN_NIVEL as Nivel
                                                    , AGN_ST_ORDEM as Ordem
                                                    , AGN_DT_ULTIMAATUCAD as UltimaAtualizacao
                                                    , AGN_IN_VALIDADECAD as ValidadeCadastroEmDias
                                                    , AGN_ST_CEI as CEI
                                                    , AGN_CH_TIPOINSCRICAO as TipoInscricao
                                                    , AGN_CH_ENQUADRAMENTO as Enquadramento
                                                    , CNAE_ST_CODIGO as CDAEId
                                                    , AGN_BO_RETERIR as ReterIR
                                                    , AGN_BO_RETERINSS as ReterINSS
                                                    , AGN_ST_URL as Url
                                                    , AGN_ST_APELIDO as Apelido
                                                    , AGN_ST_SENHA as Senha
                                                    , AGN_CH_STATUSINSCRESTADUAL as StatusInscricaoEstadual
                                                    , AGN_BO_PUBLICO as Publico
                                                    , AGN_ST_INSCRPRODUTOR as InscricaoProdutoRural
                                                    , USU_IN_INCLUSAO as Insclusao
                                                    , AGN_CH_RURALTIPOPESSOAFJ as TipoPessoaRural
                                                    , TAB05_IN_CODIGO as ESocialId
                                                    , AGN_ST_NRCAEPF as ESocialCAEPF
                                                    , AGN_ST_NRCNO as ESocialCNO
                                                    , AGN_CH_TIPODESPADUANEIRA as TipoDespesaAduaneira
                                                    , AGN_IN_TIPOCAEPF as TipoAtividadeEconomicaPF
                                                    , AGN_CH_NATRETPISCOFINS as NaturezaRetencaoPIS_CONFINS
                                                    , AGN_BO_RATEIOAFRMM as PermiteRateioAFRMM
                                                    , AGN_CH_TIPOAFRMM as TipoRateioAFRMM
                                                    , AGN_ST_PISMEI as PISMEI
                                                    , AGN_ST_CBOMEI as CBOMEI
                                                    , AGN_ST_NIF as NIF
                                                    , DFP_IN_CODIGO as FontePagadoraId
                                                    , AGN_BO_DISPENSADONIF as DispensadoNIF
                                                 FROM mgglo.glo_agentes
                                                WHERE agn_st_cgc            = :cpf
                                                  AND UPPER(agn_st_nome)    = :nameUpper"
                                             , new { cpf, nameUpper }).SingleOrDefault();
        }

        public decimal getNewIdAgente()
        {
            return _connection.Query<decimal>(@"SELECT mgdbm.pck_dbm_util.Fnc_DBM_ProximaSequencia(53) 
                                                  FROM DUAL").SingleOrDefault();
        }

        public bool Insert(Agente agente)
        {
            _connection.Execute(@"insert into MGGLO.GLO_AGENTES
                 (AGN_TAB_IN_CODIGO,        AGN_PAD_IN_CODIGO,      AGN_IN_CODIGO,            AGN_BO_CONSOLIDADOR,
                  AGN_ST_FANTASIA,          AGN_ST_NOME,            AGN_CH_TIPOPESSOAFJ,      AGN_IN_NATJURID,
                  UF_ST_SIGLA,              PA_ST_SIGLA,            MUN_IN_CODIGO,
                  AGN_ST_MUNICIPIO,         TPL_ST_SIGLA,           AGN_ST_LOGRADOURO,        AGN_ST_NUMERO,
                  AGN_ST_BAIRRO,            AGN_ST_CEP,             AGN_ST_CXPOSTAL,          AGN_ST_COMPLEMENTO,
                  AGN_ST_CEPCXPOSTAL,       AGN_ST_REFERENCIA,      AGN_ST_ENQUADRAIPI,       AGN_ST_ENQUADRAICMS,
                  AGN_ST_ENQUADRAISS,       AGN_ST_CGC,             AGN_CH_STATUSCGC,         AGN_ST_FRETEPESOVALOR,
                  PAI_AGN_TAB_IN_CODIGO,  AGN_ST_ATIVECONOMICA,     PAI_AGN_PAD_IN_CODIGO,
                  PAI_AGN_IN_CODIGO,        AGN_ST_SUFRAMA,         AGN_ST_REPARTICAO,        AGN_ST_EMAIL,
                  AGN_BO_IPISIMPLES,        AGN_BO_ICMSSIMPLES,     AGN_BO_ISSSIMPLES,        AGN_BO_SIMPLES,
                  AGN_BO_FLUXO,           AGN_IN_NIVEL,             AGN_ST_ORDEM,
                  AGN_DT_ULTIMAATUCAD,      AGN_IN_VALIDADECAD,     AGN_CH_TIPOINSCRICAO,
                  AGN_CH_ENQUADRAMENTO,     CNAE_ST_CODIGO,         AGN_BO_RETERIR,           AGN_BO_RETERINSS,
                  AGN_ST_URL,               AGN_ST_SENHA,           AGN_ST_APELIDO,           USU_IN_INCLUSAO,
                  AGN_ST_CEI,               AGN_ST_INSCRESTADUAL,   AGN_ST_INSCRMUNIC)
               values
                 (:AGN_TAB_IN_CODIGO,       :AGN_PAD_IN_CODIGO,     :AGN_IN_CODIGO,           :AGN_BO_CONSOLIDADOR,
                  :AGN_ST_FANTASIA,         :AGN_ST_NOME,           :AGN_CH_TIPOPESSOAFJ,     :AGN_IN_NATJURID,
                  :UF_ST_SIGLA,             :PA_ST_SIGLA,           :MUN_IN_CODIGO,
                  :AGN_ST_MUNICIPIO,        :TPL_ST_SIGLA,          :AGN_ST_LOGRADOURO,       :AGN_ST_NUMERO,
                  :AGN_ST_BAIRRO,           :AGN_ST_CEP,            :AGN_ST_CXPOSTAL,         :AGN_ST_COMPLEMENTO,
                  :AGN_ST_CEPCXPOSTAL,      :AGN_ST_REFERENCIA,     :AGN_ST_ENQUADRAIPI,      :AGN_ST_ENQUADRAICMS,
                  :AGN_ST_ENQUADRAISS,      :AGN_ST_CGC,            :AGN_CH_STATUSCGC,        :AGN_ST_FRETEPESOVALOR,
                  :PAI_AGN_TAB_IN_CODIGO,   :AGN_ST_ATIVECONOMICA,  :PAI_AGN_PAD_IN_CODIGO,
                  :PAI_AGN_IN_CODIGO,       :AGN_ST_SUFRAMA,        :AGN_ST_REPARTICAO,       :AGN_ST_EMAIL,
                  :AGN_BO_IPISIMPLES,       :AGN_BO_ICMSSIMPLES,    :AGN_BO_ISSSIMPLES,       :AGN_BO_SIMPLES,
                  :AGN_BO_FLUXO,          :AGN_IN_NIVEL,            :AGN_ST_ORDEM,
                  :AGN_DT_ULTIMAATUCAD,     :AGN_IN_VALIDADECAD,    :AGN_CH_TIPOINSCRICAO,
                  :AGN_CH_ENQUADRAMENTO,    :CNAE_ST_CODIGO,        :AGN_BO_RETERIR,          :AGN_BO_RETERINSS,
                  :AGN_ST_URL,              :AGN_ST_SENHA,          :AGN_ST_APELIDO,          :USU_IN_INCLUSAO,
                  :AGN_ST_CEI,              :AGN_ST_INSCRESTADUAL,  :AGN_ST_INSCRMUNIC)"
            , new {
                       AGN_TAB_IN_CODIGO = agente.Tab
                    ,  AGN_PAD_IN_CODIGO = agente.Pad
                    ,  AGN_IN_CODIGO = agente.Id
                    ,  AGN_BO_CONSOLIDADOR = agente.TipoOrganizacao
                    ,  AGN_ST_FANTASIA = agente.NomeFantasia
                    ,  AGN_ST_NOME = agente.Nome
                    ,  AGN_CH_TIPOPESSOAFJ = agente.TipoPessoa
                    ,  AGN_IN_NATJURID = agente.NaturezaJuridica
                    ,  UF_ST_SIGLA = agente.EstadoSigla
                    ,  PA_ST_SIGLA = agente.EnderecoPaisSigla
                    ,  MUN_IN_CODIGO = agente.EnderecoMunicipioId
                    ,  AGN_ST_MUNICIPIO = agente.EnderecoMunicipioNome
                    ,  TPL_ST_SIGLA = agente.EnderecoLogradouroSigla
                    ,  AGN_ST_LOGRADOURO = agente.EnderecoLogradouroNome
                    ,  AGN_ST_NUMERO = agente.EnderecoNumeroPredio
                    ,  AGN_ST_BAIRRO = agente.EnderecoNomeBairro
                    ,  AGN_ST_CEP = agente.EnderecoCEP
                    ,  AGN_ST_CXPOSTAL = agente.EnderecoCaixaPostal
                    ,  AGN_ST_COMPLEMENTO = agente.EnderecoComplemento
                    ,  AGN_ST_CEPCXPOSTAL = agente.EnderecoCEPCaixaPostal
                    ,  AGN_ST_REFERENCIA = agente.EnderecoReferencia
                    ,  AGN_ST_ENQUADRAIPI = agente.EnquadraIPI
                    ,  AGN_ST_ENQUADRAICMS = agente.EnquadraICMS
                    ,  AGN_ST_ENQUADRAISS = agente.EnquadraISS
                    ,  AGN_ST_CGC = agente.CGC
                    ,  AGN_CH_STATUSCGC = agente.CGCStatus
                    ,  AGN_ST_FRETEPESOVALOR = agente.FretePesoValor
                    ,  PAI_AGN_TAB_IN_CODIGO = agente.TabPai
                    ,  AGN_ST_ATIVECONOMICA = agente.AtividadeEconomica
                    ,  PAI_AGN_PAD_IN_CODIGO = agente.PadPai
                    ,  PAI_AGN_IN_CODIGO = agente.IdPai
                    ,  AGN_ST_SUFRAMA = agente.Suframa
                    ,  AGN_ST_REPARTICAO = agente.Reparticao
                    ,  AGN_ST_EMAIL = agente.Email
                    ,  AGN_BO_IPISIMPLES = agente.IPISimples
                    ,  AGN_BO_ICMSSIMPLES = agente.ICMSSimples
                    ,  AGN_BO_ISSSIMPLES = agente.ISSSimples
                    ,  AGN_BO_SIMPLES = agente.OpcaoSimples
                    ,  AGN_BO_FLUXO = agente.Fluxo
                    ,  AGN_IN_NIVEL = agente.Nivel
                    ,  AGN_ST_ORDEM = agente.Ordem
                    ,  AGN_DT_ULTIMAATUCAD = agente.UltimaAtualizacao
                    ,  AGN_IN_VALIDADECAD = agente.ValidadeCadastroEmDias
                    ,  AGN_CH_TIPOINSCRICAO = agente.TipoInscricao
                    ,  AGN_CH_ENQUADRAMENTO = agente.Enquadramento
                    ,  CNAE_ST_CODIGO = agente.CNAEId
                    ,  AGN_BO_RETERIR = agente.ReterIR
                    ,  AGN_BO_RETERINSS = agente.ReterINSS
                    ,  AGN_ST_URL = agente.Url
                    ,  AGN_ST_SENHA = agente.Senha
                    ,  AGN_ST_APELIDO = agente.Apelido
                    ,  USU_IN_INCLUSAO = agente.Inclusao
                    ,  AGN_ST_CEI = agente.CEI
                    ,  AGN_ST_INSCRESTADUAL = agente.InscricaoEstadual
                    ,  AGN_ST_INSCRMUNIC = agente.InscricaoMunicipal                   
                });
            return true;            
        }

        public bool Insert(EndAgente endAgente)
        {
            _connection.Execute(@"insert into MGGLO.GLO_ENDAGENTES
                 (AGN_TAB_IN_CODIGO,        AGN_PAD_IN_CODIGO,      AGN_IN_CODIGO,            ENA_IN_CODIGO,
                  PAG_IN_CODIGO,            PA_ST_SIGLA,            UF_ST_SIGLA,              TPL_ST_SIGLA,
                  TEA_ST_CODIGO,            MUN_IN_CODIGO,          ENA_ST_LOGRADOURO,        ENA_ST_NUMERO,
                  ENA_ST_BAIRRO,            ENA_ST_MUNICIPIO,       ENA_ST_CEP,               ENA_ST_CXPOSTAL,
                  ENA_ST_COMPLEMENTO,       ENA_ST_REFERENCIA,      ENA_ST_TELEFONE,          ENA_ST_FAX)
               VALUES
                 (:AGN_TAB_IN_CODIGO,       :AGN_PAD_IN_CODIGO,     :AGN_IN_CODIGO,           :ENA_IN_CODIGO,
                  :PAG_IN_CODIGO,           :PA_ST_SIGLA,           :UF_ST_SIGLA,             :TPL_ST_SIGLA,
                  :TEA_ST_CODIGO,           :MUN_IN_CODIGO,         :ENA_ST_LOGRADOURO,       :ENA_ST_NUMERO,
                  :ENA_ST_BAIRRO,           :ENA_ST_MUNICIPIO,      :ENA_ST_CEP,              :ENA_ST_CXPOSTAL,
                  :ENA_ST_COMPLEMENTO,      :ENA_ST_REFERENCIA,     :ENA_ST_TELEFONE,         :ENA_ST_FAX)"
             , new{    AGN_TAB_IN_CODIGO = endAgente.Tab
                    ,  AGN_PAD_IN_CODIGO = endAgente.Pad
                    ,  AGN_IN_CODIGO = endAgente.IdAgente
                    ,  ENA_IN_CODIGO = endAgente.IdEndereco
                    ,  PAG_IN_CODIGO = endAgente.IdPessoaContato
                    ,  PA_ST_SIGLA = endAgente.SiglaPais
                    ,  UF_ST_SIGLA = endAgente.SiglaEstado
                    ,  TPL_ST_SIGLA = endAgente.Sigla
                    ,  TEA_ST_CODIGO = endAgente.TipoEndereco
                    ,  MUN_IN_CODIGO = endAgente.idMunicipio
                    ,  ENA_ST_LOGRADOURO = endAgente.NomeLogradouro
                    ,  ENA_ST_NUMERO = endAgente.Numero
                    ,  ENA_ST_BAIRRO = endAgente.Bairro
                    ,  ENA_ST_MUNICIPIO = endAgente.Municipio
                    ,  ENA_ST_CEP = endAgente.CEP
                    ,  ENA_ST_CXPOSTAL = endAgente.CaixaPosta
                    ,  ENA_ST_COMPLEMENTO = endAgente.Complemento
                    ,  ENA_ST_REFERENCIA = endAgente.Referencia
                    ,  ENA_ST_TELEFONE = endAgente.Telefone
                    ,  ENA_ST_FAX = endAgente.Fax
                   });
            return true;
        }

        public bool InsertCliente(Cliente cliente)
        {
            _connection.Execute(@"INSERT INTO MGGLO.GLO_CLIENTE(AGN_TAB_IN_CODIGO     , AGN_PAD_IN_CODIGO  , AGN_IN_CODIGO, AGN_TAU_ST_CODIGO,
                                                                CLI_CH_CONCORDATARIO  , CLI_CH_PROCFALENCIA, CLI_BO_APLICACAOPRODUTO,
                                                                CLI_BO_FRETEAUTOMATICO, CLI_BO_PREDIOPROPRIO)
                                                        VALUES (:AGN_TAB_IN_CODIGO     , :AGN_PAD_IN_CODIGO  , :AGN_IN_CODIGO, :AGN_TAU_ST_CODIGO,
                                                                :CLI_CH_CONCORDATARIO  , :CLI_CH_PROCFALENCIA, :CLI_BO_APLICACAOPRODUTO,
                                                                :CLI_BO_FRETEAUTOMATICO, :CLI_BO_PREDIOPROPRIO)"
                                ,new
                                {
                                   AGN_TAB_IN_CODIGO = cliente.AgenteTab
                                ,  AGN_PAD_IN_CODIGO = cliente.AgentePad
                                ,  AGN_IN_CODIGO = cliente.AgenteId
                                ,  AGN_TAU_ST_CODIGO = cliente.AgenteTau
                                ,  CLI_CH_CONCORDATARIO = cliente.EmConcordate
                                ,  CLI_CH_PROCFALENCIA = cliente.EmFalencia
                                ,  CLI_BO_APLICACAOPRODUTO = cliente.ProdutoControlaAplic
                                ,  CLI_BO_FRETEAUTOMATICO = cliente.CalculaFrete
                                ,  CLI_BO_PREDIOPROPRIO = cliente.PredioPropio
                                });
            return true;
        }

        public bool InsertConta(ContaAgente contaAgente)
        {
            _connection.Execute(@"INSERT INTO MGGLO.GLO_CONTASAGNID
                          (AGN_TAB_IN_CODIGO,     AGN_PAD_IN_CODIGO,       AGN_IN_CODIGO,
                           AGN_TAU_ST_CODIGO,     CAID_BAN_IN_NUMERO,      CAID_ST_CONTACORR,
                           CAIDAGE_ST_NUMERO,     CAID_BO_PREFERENCIAL,    CAID_CH_TIPOCONTA,
                           CAID_BO_INATIVA)
                 VALUES (  :AGN_TAB_IN_CODIGO,     :AGN_PAD_IN_CODIGO,       :AGN_IN_CODIGO,
                           :AGN_TAU_ST_CODIGO,     :CAID_BAN_IN_NUMERO,      :CAID_ST_CONTACORR,
                           :CAIDAGE_ST_NUMERO,     :CAID_BO_PREFERENCIAL,    :CAID_CH_TIPOCONTA,
                           :CAID_BO_INATIVA);"
                               , new
                               {
                                  AGN_TAB_IN_CODIGO = contaAgente.AgenteTab
                                , AGN_PAD_IN_CODIGO = contaAgente.AgentePad
                                , AGN_IN_CODIGO = contaAgente.AgenteId
                                , AGN_TAU_ST_CODIGO = contaAgente.AgenteTau
                                , CAID_BAN_IN_NUMERO = contaAgente.Banco
                                , CAID_ST_CONTACORR = contaAgente.Numero
                                , CAIDAGE_ST_NUMERO = contaAgente.Agencia
                                , CAID_BO_PREFERENCIAL = contaAgente.ContaPreferencial
                                , CAID_CH_TIPOCONTA = contaAgente.TipoConta
                                , CAID_BO_INATIVA = contaAgente.ContaInativa
                               });
            return true;
        }

        public bool InsertCRM(AgenteCRM agenteCRM)
        {
            _connection.Execute(@"insert into mgglo.glo_agentes_crm
                                 (agn_tab_in_codigo,
                                  agn_pad_in_codigo,
                                  agn_in_codigo,
                                  id_integracao,
                                  data_criacao,
                                  data_alteracao)
                            values
                                 (:agn_tab_in_codigo,
                                  :agn_pad_in_codigo,
                                  :agn_in_codigo,
                                  :id_integracao,
                                  :data_criacao,
                                  :data_alteracao)"
            , new{  AGN_TAB_IN_CODIGO = agenteCRM.Tab
                  , AGN_PAD_IN_CODIGO = agenteCRM.Pad
                  , AGN_IN_CODIGO = agenteCRM.IdAgente
                  , ID_INTEGRACAO = agenteCRM.IdIntegracao
                  , DATA_CRIACAO = agenteCRM.Criacao
                  , DATA_ALTERACAO = agenteCRM.Alteracao
                  });
            return true;
        }

        public bool InsertId(AgenteId agenteId)
        {
            _connection.Execute(@"insert into MGGLO.GLO_AGENTES_ID
                                    (AGN_TAB_IN_CODIGO,  AGN_PAD_IN_CODIGO,      AGN_IN_CODIGO,            AGN_TAU_ST_CODIGO,
                                    AGN_ST_CODIGOALT,    CAT_TAB_IN_CODIGO,      CAT_PAD_IN_CODIGO,        CAT_IDE_ST_CODIGO,
                                    CAT_IN_REDUZIDO,     PLA_TAB_IN_CODIGO,      PLA_PAD_IN_CODIGO,        PLA_IDE_ST_CODIGO,
                                    PLA_IN_REDUZIDO,     AGN_BO_ATIVADOSALDO,    AGN_BO_EXCECAOFISCAL,     PRIP_ST_CODIGO,
                                    COND_TAB_IN_CODIGO,  COND_PAD_IN_CODIGO,     COND_ST_CODIGO,           AGN_CH_DATABASE,
                                    AGN_CH_STATUS,       AGN_IN_PAGTOGPS)
                                values
                                    (:AGN_TAB_IN_CODIGO,   :AGN_PAD_IN_CODIGO,      :AGN_IN_CODIGO,            :AGN_TAU_ST_CODIGO,
                                     :AGN_ST_CODIGOALT,    :CAT_TAB_IN_CODIGO,      :CAT_PAD_IN_CODIGO,        :CAT_IDE_ST_CODIGO,
                                     :CAT_IN_REDUZIDO,     :PLA_TAB_IN_CODIGO,      :PLA_PAD_IN_CODIGO,        :PLA_IDE_ST_CODIGO,
                                     :PLA_IN_REDUZIDO,     :AGN_BO_ATIVADOSALDO,    :AGN_BO_EXCECAOFISCAL,     :PRIP_ST_CODIGO,
                                     :COND_TAB_IN_CODIGO,  :COND_PAD_IN_CODIGO,     :COND_ST_CODIGO,           :AGN_CH_DATABASE,
                                     :AGN_CH_STATUS,       :AGN_IN_PAGTOGPS)"
            , new{ AGN_TAB_IN_CODIGO = agenteId.Tab
                ,  AGN_PAD_IN_CODIGO = agenteId.Pad
                ,  AGN_IN_CODIGO = agenteId.IdAgente
                ,  AGN_TAU_ST_CODIGO = agenteId.Tau
                ,  AGN_ST_CODIGOALT = agenteId.IdAlternativoAgente
                ,  CAT_TAB_IN_CODIGO = agenteId.CategoriaTab
                ,  CAT_PAD_IN_CODIGO = agenteId.CategoriaPad
                ,  CAT_IDE_ST_CODIGO = agenteId.CategoriaIdentificador
                ,  CAT_IN_REDUZIDO = agenteId.CategoriaIdReduzido
                ,  PLA_TAB_IN_CODIGO = agenteId.ContaContabilTab
                ,  PLA_PAD_IN_CODIGO = agenteId.ContaContabilPad
                ,  PLA_IDE_ST_CODIGO = agenteId.ContaContabilCodigo
                ,  PLA_IN_REDUZIDO = agenteId.ContaContabilIdReduzido
                ,  AGN_BO_ATIVADOSALDO = agenteId.AtivadoSaldo
                ,  AGN_BO_EXCECAOFISCAL = agenteId.ExcecaoFisscal
                ,  PRIP_ST_CODIGO = agenteId.PrioridadePagamentoId
                ,  COND_TAB_IN_CODIGO = agenteId.CondicaoPagamentoTab
                ,  COND_PAD_IN_CODIGO = agenteId.CondicaoPagamentoPad
                ,  COND_ST_CODIGO = agenteId.CondicaoPagamentoCodigo
                ,  AGN_CH_DATABASE = agenteId.DataCondicaoPagamento
                ,  AGN_CH_STATUS = agenteId.Status
                ,  AGN_IN_PAGTOGPS = agenteId.PagamentoGPS
                });
            return true;

        }

        public bool InsertParametroFiscal(ParametroFiscal parametroFiscal)
        {
            _connection.Execute(@"insert into MGGLO.GLO_PFISCAL_AGENTE
                                   (AGN_TAB_IN_CODIGO,      AGN_PAD_IN_CODIGO,     AGN_IN_CODIGO,           AGN_DT_INIVIGENCIA,
                                    AGN_BO_ENQUADRAIPI,     AGN_BO_ENQUADRAICMS,   AGN_BO_ENQUADRAISS,      AGN_BO_RETERIR,
                                    AGN_BO_RETERINSS,       AGN_BO_SIMPLES,        AGN_BO_IPISIMPLES,       AGN_BO_ICMSSIMPLES,
                                    AGN_BO_ISSSIMPLES,      AGN_BO_ESCRITURAR,     AGN_ST_OBS)
                                  values
                                   (:AGN_TAB_IN_CODIGO,      :AGN_PAD_IN_CODIGO,     :AGN_IN_CODIGO,           :AGN_DT_INIVIGENCIA,
                                    :AGN_BO_ENQUADRAIPI,     :AGN_BO_ENQUADRAICMS,   :AGN_BO_ENQUADRAISS,      :AGN_BO_RETERIR,
                                    :AGN_BO_RETERINSS,       :AGN_BO_SIMPLES,        :AGN_BO_IPISIMPLES,       :AGN_BO_ICMSSIMPLES,
                                    :AGN_BO_ISSSIMPLES,      :AGN_BO_ESCRITURAR,     :AGN_ST_OBS)"
                                , new {
                                          AGN_TAB_IN_CODIGO = parametroFiscal.Tab
                                        , AGN_PAD_IN_CODIGO = parametroFiscal.Pad
                                        , AGN_IN_CODIGO = parametroFiscal.AgenteId
                                        , AGN_DT_INIVIGENCIA = parametroFiscal.InicioVigencia
                                        , AGN_BO_ENQUADRAIPI = parametroFiscal.EquadraIPI
                                        , AGN_BO_ENQUADRAICMS = parametroFiscal.EquadraICMS
                                        , AGN_BO_ENQUADRAISS = parametroFiscal.EquadraISS
                                        , AGN_BO_RETERIR = parametroFiscal.ReterIR
                                        , AGN_BO_RETERINSS = parametroFiscal.ReterINSS
                                        , AGN_BO_SIMPLES = parametroFiscal.OpcaoSimples
                                        , AGN_BO_IPISIMPLES = parametroFiscal.SimplesIPI
                                        , AGN_BO_ICMSSIMPLES = parametroFiscal.SimplesICMSS
                                        , AGN_BO_ISSSIMPLES = parametroFiscal.SimplesISS
                                        , AGN_ST_OBS = parametroFiscal.Observacao
                                        , AGN_BO_ESCRITURAR = parametroFiscal.EscriturarMovFiscal
                                    });
            return true;
        }

        public bool InsertPessoa(PessoaAgente pessoaAgente)
        {
            _connection.Execute(@"INSERT INTO mgglo.glo_pessoa_agentes (  agn_tab_in_codigo   , agn_pad_in_codigo    , agn_in_codigo
                                                                        , pag_in_codigo       , pag_st_nome          , pag_st_email
                                                                        , pag_ch_tipo         , tpr_in_codigo        , pag_ch_sexo
                                                                        , pag_ch_estadocivil  , pag_bo_respfiscal    , pag_bo_respfinanc)
                                             VALUES ( :agn_tab_in_codigo   , :agn_pad_in_codigo    , :agn_in_codigo
                                                    , :pag_in_codigo       , :pag_st_nome          , :pag_st_email
                                                    , :pag_ch_tipo         , :tpr_in_codigo        , :pag_ch_sexo
                                                    , :pag_ch_estadocivil  , :pag_bo_respfiscal    , :pag_bo_respfinanc);"
                                  , new
                                  {
                                      AGN_TAB_IN_CODIGO = pessoaAgente.Tab
                                    , AGN_PAD_IN_CODIGO = pessoaAgente.Pad
                                    , AGN_IN_CODIGO = pessoaAgente.AgenteId
                                    , PAG_IN_CODIGO = pessoaAgente.Id
                                    , PAG_ST_NOME = pessoaAgente.Nome
                                    , PAG_ST_EMAIL = pessoaAgente.Email
                                    , PAG_CH_TIPO = pessoaAgente.Tipo
                                    , TPR_IN_CODIGO = pessoaAgente.RelacionamentoTipo
                                    , PAG_CH_SEXO = pessoaAgente.Genero
                                    , PAG_CH_ESTADOCIVIL = pessoaAgente.EstadoCivil
                                    , PAG_BO_RESPFISCAL = pessoaAgente.ResponsavelFiscal
                                    , PAG_BO_RESPFINANC = pessoaAgente.ResponsavelFinanceiro
                                  });
            return true;
        }

        public bool InsertPessoaFisica(PessoaFisica pessoaFisica)
        {
            _connection.Execute(@"insert into MGGLO.GLO_PESSOA_FISICA
                                       (AGN_TAB_IN_CODIGO,      AGN_PAD_IN_CODIGO,      AGN_IN_CODIGO,          AGN_CH_STATUSCPF,
                                        AGN_ST_CPF,             AGN_ST_RG,              AGN_DT_NASCIMENTO,       
                                        AGN_ST_ESTADONASCIM,    AGN_ST_LOCALTRABALHO,   AGN_ST_CARGOPROFISS, 
                                        AGN_CH_ESTCIVIL,        AGN_CH_REGIMECASAMENTO,
                                        AGN_CH_SEXO,            AGN_ST_CONJUGE,
                                        AGN_CH_TIPO,            PAI_AGN_TAB_IN_CODIGO,  PAI_AGN_PAD_IN_CODIGO,  PAI_AGN_IN_CODIGO,
                                        PAI_AGN_CH_TIPO,        AGN_DT_EXPRG,           AGN_ST_ORGEMISSORRG,
                       
                                        AGN_ST_LOCALNASCIM,     AGN_ST_NOMEPAI,         AGN_ST_NOMEMAE,         AGN_ST_NACIONALIDADE,
                                        AGN_DT_ADMISSAO,        AGN_ST_CARTTRABALHO,    AGN_ST_TELLOCTRAB,      AGN_RE_SALARIO,
                                        AGN_ST_LOCTRABCONJ,     AGN_ST_TELLOCTRABCONJ)
                                   values
                                       (:AGN_TAB_IN_CODIGO,      :AGN_PAD_IN_CODIGO,      :AGN_IN_CODIGO,          :AGN_CH_STATUSCPF,
                                        :AGN_ST_CPF,             :AGN_ST_RG,              :AGN_DT_NASCIMENTO,       
                                        :AGN_ST_ESTADONASCIM,    :AGN_ST_LOCALTRABALHO,   :AGN_ST_CARGOPROFISS, 
                                        :AGN_CH_ESTCIVIL,        :AGN_CH_REGIMECASAMENTO,
                                        :AGN_CH_SEXO,            :AGN_ST_CONJUGE,
                                        :AGN_CH_TIPO,            :PAI_AGN_TAB_IN_CODIGO,  :PAI_AGN_PAD_IN_CODIGO,  :PAI_AGN_IN_CODIGO,
                                        :PAI_AGN_CH_TIPO,        :AGN_DT_EXPRG,           :AGN_ST_ORGEMISSORRG,

                                        :AGN_ST_LOCALNASCIM,     :AGN_ST_NOMEPAI,         :AGN_ST_NOMEMAE,         :AGN_ST_NACIONALIDADE,
                                        :AGN_DT_ADMISSAO,        :AGN_ST_CARTTRABALHO,    :AGN_ST_TELLOCTRAB,      :AGN_RE_SALARIO,
                                        :AGN_ST_LOCTRABCONJ,     :AGN_ST_TELLOCTRABCONJ)"
                       , new{  AGN_TAB_IN_CODIGO = pessoaFisica.AgenteTab
                            ,  AGN_PAD_IN_CODIGO = pessoaFisica.AgentePad
                            ,  AGN_IN_CODIGO = pessoaFisica.AgenteId
                            ,  AGN_CH_STATUSCPF = pessoaFisica.AgenteStatusCPF
                            ,  AGN_ST_CPF = pessoaFisica.CPF
                            ,  AGN_ST_RG = pessoaFisica.RG
                            ,  AGN_DT_NASCIMENTO = pessoaFisica.Nascimento
                            ,  AGN_ST_ESTADONASCIM = pessoaFisica.EstadoNascimento
                            ,  AGN_ST_LOCALTRABALHO = pessoaFisica.LocalTrabalho
                            ,  AGN_ST_CARGOPROFISS = pessoaFisica.Cargo
                            ,  AGN_CH_ESTCIVIL = pessoaFisica.EstadoCivil
                            ,  AGN_CH_REGIMECASAMENTO = pessoaFisica.RegimeCasamento
                            ,  AGN_CH_SEXO = pessoaFisica.Genero
                            ,  AGN_ST_CONJUGE = pessoaFisica.Conjuge
                            ,  AGN_CH_TIPO = pessoaFisica.TipoPessoa
                            ,  PAI_AGN_TAB_IN_CODIGO = pessoaFisica.AgentePaiTab
                            ,  PAI_AGN_PAD_IN_CODIGO = pessoaFisica.AgentePaiPad
                            ,  PAI_AGN_IN_CODIGO = pessoaFisica.AgentePaiId
                            ,  PAI_AGN_CH_TIPO = pessoaFisica.AgentePaiTipo
                            ,  AGN_DT_EXPRG = pessoaFisica.RGDataEmissao
                            ,  AGN_ST_ORGEMISSORRG = pessoaFisica.RGOrgaoEmissor
                            ,  AGN_ST_LOCALNASCIM = pessoaFisica.LocalNascimento
                            ,  AGN_ST_NOMEPAI = pessoaFisica.NomePai
                            ,  AGN_ST_NOMEMAE = pessoaFisica.NomeMae
                            ,  AGN_ST_NACIONALIDADE = pessoaFisica.Nacionalidade
                            ,  AGN_DT_ADMISSAO = pessoaFisica.DataAdmissao
                            ,  AGN_ST_CARTTRABALHO = pessoaFisica.CarteiraTrabalho
                            ,  AGN_ST_TELLOCTRAB = pessoaFisica.TelefoneTrabalho
                            ,  AGN_RE_SALARIO = pessoaFisica.Salario
                            , AGN_ST_LOCTRABCONJ = pessoaFisica.LocalTrabalhoConjuge
                            , AGN_ST_TELLOCTRABCONJ = pessoaFisica.TelefoneTrabalhoConjuge
                            });
            return true;
        }

        public bool InsertTelefone(TelefoneAgente telefoneAgente)
        {
            _connection.Execute(@"insert into MGGLO.GLO_TEL_AGENTES
                                        (AGN_TAB_IN_CODIGO
                                       , AGN_PAD_IN_CODIGO
                                       , AGN_IN_CODIGO
                                       , TEA_ST_TELEFONE
                                       , TEA_ST_TIPO)
                                   values
                                      (:AGN_TAB_IN_CODIGO
                                     , :AGN_PAD_IN_CODIGO
                                     , :AGN_IN_CODIGO
                                     , :TEA_ST_TELEFONE
                                     , :TEA_ST_TIPO);"
                                , new
                                {
                                      AGN_TAB_IN_CODIGO = telefoneAgente.Tab
                                    , AGN_PAD_IN_CODIGO = telefoneAgente.Pad
                                    , AGN_IN_CODIGO = telefoneAgente.IdAgente
                                    , TEA_ST_TELEFONE = telefoneAgente.Numero
                                    , TEA_ST_TIPO = telefoneAgente.Tipo
                                });
            return true;
        }

        public bool Edit(PessoaFisica pessoaFisica)
        {
            _connection.Execute(@"UPDATE mgglo.glo_pessoa_fisica fis
                                    SET fis.agn_st_cpf           = :ProCPF
                                    , fis.agn_ch_statuscpf       = :ProStatusCPF
                                    , fis.agn_st_rg              = :ProRG
                                    , fis.agn_dt_exprg           = :ProRGDataExp
                                    , fis.agn_st_orgemissorrg    = :ProRGOrgEmi
                                    , fis.agn_dt_nascimento      = :ProNascimento
                                    , fis.agn_st_cargoprofiss    = :ProCargo
                                    , fis.agn_ch_estcivil        = :ProEstadoCivil
                                    , fis.agn_ch_regimecasamento = :ProRegCas
                                    , fis.agn_ch_sexo            = :ProSexo
                                    , fis.agn_st_conjuge         = :ProConjuge
                                    , fis.agn_st_localtrabalho   = :ProEmp
                                    , fis.agn_st_localnascim     = :ProStLocNasc
                                    , fis.agn_st_nomepai         = :ProStNomePai
                                    , fis.agn_st_nomemae         = :ProStNomeMae
                                    , fis.agn_st_nacionalidade   = :ProStNac
                                    , fis.agn_dt_admissao        = :ProDtAdmissao
                                    , fis.agn_st_carttrabalho    = :ProStCartTrab
                                    , fis.agn_st_telloctrab      = :ProStTelLocTrab
                                    , fis.agn_re_salario         = :ProReSalario
                                    , fis.agn_st_loctrabconj     = :ProEmpCje
                                    , fis.agn_st_telloctrabconj  = :ProStTelLocTrabCje

                                WHERE fis.agn_in_codigo          = :ProAgnInCodigo
                                  AND fis.agn_ch_tipo            = :ProAgnTipo"
                            , new
                            {
                                ProCPF = pessoaFisica.CPF,
                                ProStatusCPF = pessoaFisica.AgenteStatusCPF,
                                ProRG = pessoaFisica.RG,
                                ProRGDataExp = pessoaFisica.RGDataEmissao,
                                ProRGOrgEmi = pessoaFisica.RGOrgaoEmissor,
                                ProNascimento = pessoaFisica.Nascimento,
                                ProCargo = pessoaFisica.Cargo,
                                ProEstadoCivil = pessoaFisica.EstadoCivil,
                                ProRegCas = pessoaFisica.RegimeCasamento,
                                ProSexo = pessoaFisica.Genero,
                                ProConjuge = pessoaFisica.Conjuge,
                                ProEmp = pessoaFisica.LocalTrabalho,
                                ProStLocNasc = pessoaFisica.LocalNascimento,
                                ProStNomePai = pessoaFisica.NomePai,
                                ProStNomeMae = pessoaFisica.NomeMae,
                                ProStNac = pessoaFisica.Nacionalidade,
                                ProDtAdmissao = pessoaFisica.DataAdmissao,
                                ProStCartTrab = pessoaFisica.CarteiraTrabalho,
                                ProStTelLocTrab = pessoaFisica.TelefoneTrabalho,
                                ProReSalario = pessoaFisica.Salario,
                                ProEmpCje = pessoaFisica.LocalTrabalhoConjuge,
                                ProStTelLocTrabCje = pessoaFisica.TelefoneTrabalhoConjuge,
                                ProAgnInCodigo = pessoaFisica.AgenteId,
                                ProAgnTipo = pessoaFisica.TipoPessoa
                            });
            return true;
        }

        public bool DeleteConjuge(Agente agente)
        {
            _connection.Execute(@"DELETE mgglo.glo_pessoa_fisica fis
                                   WHERE fis.agn_tab_in_codigo = :ProAgnTabCodigo
                                     AND fis.agn_pad_in_codigo = :ProAgnPadCodigo
                                     AND fis.agn_in_codigo     = :ProAgnInCodigo
                                     AND fis.agn_ch_tipo         = 'C';  -- cônjuge",
                                new {
                                     ProAgnTabCodigo = agente.Tab
                                   , ProAgnPadCodigo = agente.Pad
                                   , ProAgnInCodigo = agente.Id
                                    });
            return true;
        }

        public bool DeleteAllTelefone(Agente agente)
        {
            _connection.Execute(@"DELETE mgglo.glo_tel_agentes tag
                                   WHERE tag.agn_tab_in_codigo = :ProAgnTabCodigo
                                     AND tag.agn_pad_in_codigo = :ProAgnPadCodigo
                                     AND tag.agn_in_codigo     = :ProAgnInCodigo",
                                new
                                {
                                    ProAgnTabCodigo = agente.Tab
                                   ,ProAgnPadCodigo = agente.Pad
                                   ,ProAgnInCodigo = agente.Id
                                });
            return true;
        }

        public bool EditCampoEspecifico(AgenteCampoEspecifico campoEspecifico)
        {
            _connection.Execute(@"UPDATE mgglo.glo_agentecampoespecifico e
                                  SET e.agn_st_pacto_nupcial = AgnStPactoNupcial
                                    , e.agn_dt_pacto_nupcial = AgnDtPactoNupcial
                                    , e.agn_dt_casamento     = AgnDtCasamento
                                    , e.agn_st_tabelionato   = AgnStTabelionato
                                    , e.agn_st_registro      = AgnStRegistro
                                    , e.agn_st_livro         = AgnStLivro
                                    , e.agn_st_folhas        = AgnStFolha
                                WHERE e.agn_tab_in_codigo = ProAgnTabCodigo
                                  AND e.agn_pad_in_codigo = ProAgnPadCodigo
                                  AND e.agn_in_codigo     = ProAgnInCodigo;"
                                ,new
                                {
                                  AgnStPactoNupcial = campoEspecifico.AgenteTab
                                , AgnDtPactoNupcial = campoEspecifico.AgentePad
                                , AgnDtCasamento = campoEspecifico.AgenteId
                                , AgnStTabelionato = campoEspecifico.PactoNupcial
                                , AgnStRegistro = campoEspecifico.DataPacto
                                , AgnStLivro = campoEspecifico.DataCasamento
                                , AgnStFolha = campoEspecifico.Tabelionato
                                , ProAgnTabCodigo = campoEspecifico.Registro
                                , ProAgnPadCodigo = campoEspecifico.Livro
                                , ProAgnInCodigo = campoEspecifico.Folhas
                                });
            return true;
        }

        public AgenteId getAgenteId(Agente agente)
        {
            return _connection.Query<AgenteId>(@"SELECT i.agn_tab_in_codigo as Tab
                                                       ,i.agn_pad_in_codigo as Pad
                                                       ,i.agn_in_codigo     as IdAgente
                                                       ,i.agn_tau_st_codigo as Tau
                                                    FROM mgglo.glo_agentes_id i
                                                    WHERE i.agn_tab_in_codigo = :ProAgnTabCodigo
                                                    AND i.agn_pad_in_codigo = :ProAgnPadCodigo
                                                    AND i.agn_in_codigo     = :ProAgnInCodigo
                                                    AND i.agn_tau_st_codigo = 'C'"
                                                , new {
                                                    ProAgnTabCodigo = agente.Tab,
                                                    ProAgnPadCodigo = agente.Pad,
                                                    ProAgnInCodigo  = agente.Id
                                                }).SingleOrDefault();
        }
    }
}
