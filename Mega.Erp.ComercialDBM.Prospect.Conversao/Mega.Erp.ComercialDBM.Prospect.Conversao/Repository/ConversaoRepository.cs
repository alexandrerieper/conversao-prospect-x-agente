﻿using Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces;
using System.Data.Common;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Models;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Repository
{
    public class ConversaoRepository : IConversaoRepository
    {
        private DbConnection _connect;
        private DbTransaction _transaction;

        public ConversaoRepository(DbConnection connect)
        {
            _connect = connect;
        }

        public bool Delete(Prospects.Conversao.Models.Prospect prospect)
        {
            throw new NotImplementedException();
        }

        public bool Edit(Prospects.Conversao.Models.Prospect prospect)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Models.Prospect> Get()
        {
            throw new NotImplementedException();
        }

        public Models.Prospect Get(int id)
        {
            throw new NotImplementedException();
        }

        public bool Insert(Prospects.Conversao.Models.Prospect prospect)
        {
            throw new NotImplementedException();
        }

        public void BeginTransaction()
        {
            _transaction = _connect.BeginTransaction();
        }
        public void Rollback()
        {
            _transaction.Rollback();
        }
        public void Commit()
        {
            _transaction.Commit();
        }
    }
}
