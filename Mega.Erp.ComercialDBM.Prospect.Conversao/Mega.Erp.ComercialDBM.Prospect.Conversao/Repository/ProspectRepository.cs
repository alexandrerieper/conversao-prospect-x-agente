﻿using Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Models;
using System.Data.Common;
using Dapper;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Repository
{
    public class ProspectRepository : IProspectRepository
    {
        private string _selectProspectSimple => @"SELECT  PRO_IN_CODIGO          AS Id
                                                        , PRO_ST_NOME            AS Nome
                                                        , PRO_ST_FANTASIA        AS Fantasia
                                                        , PRO_CH_TIPOPESSOA      AS TipoPessoa
                                                        , PRO_CH_SEXO            AS Sexo
                                                        , PRO_CH_ESTADOCIVIL     AS EstadoCivil
                                                        , PRO_ST_EMAIL           AS EMail
                                                        , PRO_ST_HOMEPAGE        AS Site
                                                        , PRO_ST_RG              AS Rg
                                                        , PRO_ST_CPF             AS Cpf
                                                        , PRO_ST_CNPJ            AS Cnpj
                                                        , AGN_IN_CODIGO          AS AgnIn
                                                        , BAN_IN_NUMERO          AS NumeroBanco
                                                        , PRO_ST_AGENCIA         AS AgenciaBanco
                                                        , PRO_ST_CONTACORRENTE   AS ContaBanco
                                                    FROM MGCMR.CMR_PROSPECT";


        private string _selectProspect => @"SELECT PRO_IN_CODIGO   AS Id
                                          , PRO_ST_NOME            AS Nome
                                          , PRO_ST_FANTASIA        AS Fantasia
                                          , PRO_CH_TIPOPESSOA      AS TipoPessoa
                                          , PRO_CH_SEXO            AS Sexo
                                          , PRO_CH_ESTADOCIVIL     AS EstadoCivil
                                          , PRO_IN_NROFILHO        AS NumeroFilhos
                                          , PRO_ST_EMAIL           AS EMail
                                          , PRO_ST_HOMEPAGE        AS Site
                                          , PRO_ST_RG              AS Rg
                                          , PRO_ST_ORGEMISSORRG    AS OrgaoEmissorRg
                                          , PRO_DT_EXPRG           AS DataExpedicaoRg
                                          , PRO_ST_CPF             AS Cpf
                                          , PRO_CH_STATUSCPF       AS StatusCpf
                                          , PRO_ST_CNPJ            AS Cnpj
                                          , PRO_ST_INSCRESTADUAL   AS InscricaoEstadual
                                          , PRO_ST_INSCRMUNIC      AS InscricaoMunicipal
                                          , PRO_ST_CEI             AS Cei
                                          , PRO_DT_NASCIMENTO      AS DataNascimento
                                          , PRO_ST_LOCALNASCIM     AS LocalNascimento
                                          , PRO_ST_NACIONALIDADE   AS Nacionalidade
                                          , PRO_ST_NOMEPAI         AS NomePai
                                          , PRO_ST_NOMEMAE         AS NomeMae
                                          , PRO_ST_EMPRESA         AS EmpresaTrabalho
                                          , PRO_DT_ADMISSAO        AS DataAdmissaoTrabalho
                                          , PRO_ST_CARTTRABALHO    AS CarteiraTrabalho
                                          , PRO_ST_EMPRESA         AS LocalTrabalho
                                          , PRO_ST_TELLOCTRAB      AS TelefoneLocalTrabalho
                                          , PRO_RE_SALARIO         AS SalarioTrabalho
                                          , PRO_CH_REGIMECASAMENTO AS RegimeCasamento
                                          , PRO_DT_CASAMENTO       AS DataCasamento
                                          , PRO_ST_PACTO_NUPCIAL   AS PactoNupcial
                                          , PRO_DT_REGISTRO        AS DataPactoNupcial
                                          , PRO_ST_TABELIONATO     AS TabelionatoCasamento
                                          , PRO_ST_REGISTRO        AS RegistroCasamento
                                          , PRO_ST_LIVRO           AS LivroCasamento
                                          , PRO_ST_FOLHA           AS FolhaCasamento
                                          , PRO_ST_CONJUGE         AS NomeConjuge
                                          , PRO_CH_CJSEXO          AS SexoCje
                                          , PRO_ST_CJEEMAIL        AS EMailCje
                                          , PRO_ST_CJERG           AS RgCje
                                          , PRO_ST_ORGEMISSORRGCJE AS OrgaoEmissorRgCje
                                          , PRO_DT_EXPRGCJE        AS DataExpedicaoRgCje
                                          , PRO_ST_CJECPF          AS CpfCje
                                          , PRO_CH_CJESTATUSCPF    AS StatusCpfCje
                                          , PRO_DT_NASCCONJUGE     AS DataNascimentoCje
                                          , PRO_ST_CJLOCALNASCIM   AS LocalNascimentoCje
                                          , PRO_ST_CJNACIONALIDADE AS NacionalidadeCje
                                          , PRO_ST_CJNOMEPAI       AS NomePaiCje
                                          , PRO_ST_CJNOMEMAE       AS NomeMaeCje
                                          , PRO_ST_CJEEMPRESA      AS EmpresaTrabalhoCje
                                          , PRO_DT_CJADMISSAO      AS DataAdmissaoTrabalhoCje
                                          , PRO_ST_CJCARTTRABALHO  AS CarteiraTrabalhoCje
                                          , PRO_ST_CJLOCALTRABALHO AS LocalTrabalhoCje
                                          , PRO_ST_CJTELLOCTRAB    AS TelefoneLocalTrabalhoCje
                                          , PRO_RE_CJSALARIO       AS SalarioTrabalhoCje
                                          , AGN_TAB_IN_CODIGO      AS AgnTab
                                          , AGN_PAD_IN_CODIGO      AS AgnPad
                                          , AGN_IN_CODIGO          AS AgnIn
                                          , BAN_IN_NUMERO          AS NumeroBanco
                                          , PRO_ST_AGENCIA         AS AgenciaBanco
                                          , PRO_ST_CONTACORRENTE   AS ContaBanco
                                          , ID_INTEGRACAO          AS IdIntegracao
                                          , CAR_IN_CODIGO          AS CargoProspect
                                          , CJE_IN_PROFISSAO       AS CargoCje
                                       FROM MGCMR.CMR_PROSPECT";
        private string _selectEndereco => @"SELECT PEN_IN_CODIGO       AS Id
                                                 , PRO_IN_CODIGO       AS IdProspect
                                                 , PA_ST_SIGLA         AS SiglaPais
                                                 , UF_ST_SIGLA         AS SiglaEstado                                                  
                                                 , MUN_IN_CODIGO       AS CodigoMunicipio
                                                 , TPL_ST_SIGLA        AS SiglaRua
                                                 , PEN_ST_LOGRADOURO   AS Endereco
                                                 , PEN_ST_BAIRRO       AS Bairro
                                                 , PEN_ST_NUMERO       AS Numero
                                                 , PEN_ST_MUNICIPIO    AS Municipio
                                                 , PEN_ST_CEP          AS Cep
                                                 , PEN_ST_COMPLEMENTO  AS Complemento
                                                 , PEN_CH_TIPOENDERECO AS TipoEnderecoAgente
                                                 , TEA_ST_CODIGO       AS TipoEnderecoProspect
                                              FROM MGCMR.CMR_PROSPECT_ENDERECO PE
                                             WHERE PE.PEN_CH_ENDERECO = 'P'  -- INDICA ENDEREÇO DO PROSPECT";
        private string _selectTelefone => @"SELECT TEL_IN_CODIGO AS Id
                                                 , PRO_IN_CODIGO AS IdProspect
                                                 , TEL_IN_DDD    AS Ddd
                                                 , TEL_IN_NUMERO AS Numero
                                                 , TEL_CH_TIPO   AS Tipo
                                              FROM MGCMR.CMR_PROSPECT_TELEFONE";
        private string _selectCargo => @"SELECT CAR_IN_CODIGO AS Id
                                              , CAR_ST_NOME   AS Nome
                                           FROM MGGLO.GLO_CARGO";

        private DbConnection _connection;

        public ProspectRepository(DbConnection connection)
        {
            _connection = connection;
        }
        public IEnumerable<Models.Prospect> Get()
        {
            return _connection.Query<Models.Prospect>($@"{_selectProspectSimple}");
        }

        public Models.Prospect Get(decimal idProspect)
        {
            return _connection.Query<Models.Prospect, Cargo, Cargo, Models.Prospect>($@"{_selectProspect}
                                                                                    where PRO_IN_CODIGO = :idProspect",
                                                                                    (prospect, cargoProspect, cargoCje) =>
                                                                                    {
                                                                                        prospect.CargoProspect = cargoProspect;
                                                                                        prospect.CargoCje = cargoCje;
                                                                                        return prospect;
                                                                                    },
                                                                                    new {idProspect}
                                                                                    , splitOn: "CargoProspect, CargoCje").Single();
        }

        public bool Insert(Models.Prospect insert)
        {
            throw new NotImplementedException();
        }

        public string GetNomeCargo(decimal idCargo)
        {
            return _connection.Query<string>($@"{_selectCargo}
                                            where CAR_IN_CODIGO = :idCargo",
                                            new { idCargo }).SingleOrDefault();
        }

        public IEnumerable<TelefoneProspect> GetTelefones(decimal idProspect)
        {
            return _connection.Query<TelefoneProspect>($@"{_selectTelefone}
                                                        where pro_in_codigo = :idProspect",
                                                        new { idProspect });

        }

        public IEnumerable<EnderecoProspect> GetEnderecos(decimal idProspect)
        {
            return _connection.Query<EnderecoProspect>($@"{_selectEndereco}
                                                        and pro_in_codigo = :idProspect",
                                                        new { idProspect });
        }

        public bool Edit(Models.Prospect prospect)
        {
            _connection.Execute(@"UPDATE mgcmr.cmr_prospect p
                                    SET p.id_integracao = :IdIntegracaoCli
                                       ,p.agn_tab_in_codigo = :ProAgnTabCodigo
                                       ,p.agn_pad_in_codigo = :ProAgnPadCodigo
                                       ,p.agn_in_codigo     = :ProAgnInCodigo
                                  WHERE pro_in_codigo   = :pPro_In_Codigo"
                                , new
                                {
                                    IdIntegracaoCli = prospect.IdIntegracao,
                                    ProAgnTabCodigo = prospect.AgnTab,
                                    ProAgnPadCodigo = prospect.AgnPad,
                                    ProAgnInCodigo = prospect.AgnIn,
                                    pPro_In_Codigo = prospect.Id
                                });
            return true;
        }

        public IEnumerable<EnderecoProspect> GetEnderecosConjuge(decimal idProspect)
        {
            return _connection.Query<EnderecoProspect>($@"{_selectEndereco}
                                                        where pro_in_codigo = :idProspect
                                                          and pen_ch_endereco   = 'C'",
                                                        new { idProspect });
        }
    }
}
