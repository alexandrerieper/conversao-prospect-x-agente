﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Models;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Services;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Controllers
{
    [Route("api/[controller]")]
    public class ConversaoController : Controller
    {
        private ConversaoService _service;

        public ConversaoController(ConversaoService service)
        {
            _service = service;
        }

        // GET api/conversao/prospect
        [HttpGet]
        public string GetTeste()
        {
            return "Conversão Prospect X Agente iniciado com sucesso";//_service.Get();
        }

        // GET api/conversao/prospect
        [HttpGet("conversor/{idProspect}/{padOrg}/{sincroniza}")]
        public void Conversor(decimal idProspect, decimal padOrg, bool sincroniza)
        {
            _service.ProspectToAgente(idProspect, padOrg, sincroniza);
        }

        // GET api/conversao/prospect
        [HttpGet("agente/{id}")]
        public Agente GetAgente(decimal id)
        {
            return _service.GetAgente(id);
        }

        // GET api/conversao/prospect
        [HttpGet("prospect")]
        public IEnumerable<Models.Prospect> Get()
        {
            return _service.Get();//_service.Get();
        }

        // GET api/conversao/prospect
        [HttpGet("prospect/{id}")]
        public Models.Prospect Get(decimal id)
        {
            return _service.Get(id);//_service.Get();
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
