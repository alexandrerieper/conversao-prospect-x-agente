﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data.Common;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces.Services;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Models;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Services
{
    public class ConversaoService
    {
        private IConversaoRepository _repository;
        private IProspectService _prospectService;
        private IAgenteService _agenteService;

        public ConversaoService(IConversaoRepository repository
                              , IProspectService prospectService
                              , IAgenteService agenteService)
        {
            _repository = repository;
            _prospectService = prospectService;
            _agenteService = agenteService;
        }

        public Agente GetAgente(decimal id)
        {
            return _agenteService.GetById(id);
        }

        public Models.Prospect Get(int id)
        {
            return _repository.Get(id);
        }

        public IEnumerable<Models.Prospect> Get()
        {
            return _prospectService.Get();
        }

        public Models.Prospect Get(decimal idProspect)
        {
            return _prospectService.Get(idProspect);
        }

        /// <summary>
        /// Teste bla bla bla
        /// </summary>
        /// <param name="idProspect"></param>
        /// <returns>1 - ididididd</returns>
        public decimal ProspectToAgente(decimal idProspect, decimal padOrg, bool sincroniza)
        {
            Models.Prospect prospect = _prospectService.Get(idProspect);
            if (prospect is null)
                return 1; //prospect não encontrado
            
            Agente agente = _agenteService.GetById(prospect.AgnIn);
            if((agente == null) && (prospect.TipoPessoa == "F"))
            {
                agente = _agenteService.GetByNomeCPF(prospect.Nome, prospect.Cpf);
            }

            if ((agente == null) && (prospect.TipoPessoa == "J"))
            {
                agente = _agenteService.GetByNomeCNPJ(prospect.Nome, prospect.Cnpj);
            }

            _repository.BeginTransaction();
            if (agente == null)
            {   //Cria novo agente
                agente = new Agente();
                IEnumerable<EnderecoProspect> enderecos = _prospectService.GetEnderecos(prospect.Id);

                if ((prospect.TipoPessoa != "F") && (prospect.TipoPessoa != "J"))
                {
                    return 6;//Tipo de pessoa não definido
                }

                if (enderecos.Count() == 0)
                {
                    if (_prospectService.GetEnderecosConjuge(prospect.Id).Count() > 0)
                        return 5; //Endereço cadastrado está marcado como conjuge
                    return 4; //Endereço de prospect não cadastrado
                }

                prospect.AgnPad = padOrg;
                agente = DeparaProspectAgente(prospect);                
                _agenteService.Insert(agente);
                prospect.AgnIn = agente.Id;
                prospect.AgnTab = agente.Tab;

                decimal seq = 1;
                foreach (EnderecoProspect endereco in enderecos)
                {
                    EndAgente endAgente = DeparaEnderecos(endereco, agente, seq);
                    _agenteService.InsertEndereco(endAgente);
                    seq++;
                }

                //_agenteService.InsertCRM( DeparaCRM(prospect) );
                _agenteService.InsertId( DeparaId(agente) );

                if (prospect.TipoPessoa == "F")
                {
                    _agenteService.InsertPessoaFisica(DeparaPessoaFisica(prospect, false));

                    if ((prospect.EstadoCivil == "C") || (prospect.EstadoCivil == "A")){
                        _agenteService.InsertPessoaFisica(DeparaPessoaFisica(prospect, true));
                    }
                }

                _agenteService.InsertCliente(DeparaCliente(agente));

                //insertParametroFiscal
                //InsertTelefones
                
                //Para cada email
                //  insiro um registro em pessoa_agente                

                //Se possui dados bancarios
                //inserre contasagnid

                //Se possui pacto nupcial
                // Insere campoAgenteEspecifico
                
                _prospectService.Edit(prospect);
                
            }
            else //Editar agente pois já existe
            {
                if (sincroniza)
                {
                    if (prospect.TipoPessoa == "F")
                    {
                        if ((prospect.Nome == agente.Nome) && (prospect.Cpf == agente.CGC))
                            return 7; //CPF informado pertence a outro agente
                    }
                    else
                    {
                        if ((prospect.Nome == agente.Nome) && (prospect.Cnpj == agente.CGC))
                            return 8; //CPF informado pertence a outro agente
                    }

                    _agenteService.Edit(DeparaProspectAgente(prospect));
                    _agenteService.DeleteAllEnderecos(agente);
                    decimal seq = 1;
                    foreach (EnderecoProspect endereco in _prospectService.GetEnderecos(prospect.Id))
                    {
                        EndAgente endAgente = DeparaEnderecos(endereco, agente, seq);
                        _agenteService.InsertEndereco(endAgente);
                        seq++;
                    }

                    if (_agenteService.GetAgenteId(agente) == null)
                        _agenteService.InsertId(DeparaId(agente));

                    if (prospect.TipoPessoa == "F")
                    {
                        _agenteService.Edit(DeparaPessoaFisica(prospect, false));

                        if ((prospect.EstadoCivil == "C") || (prospect.EstadoCivil == "A"))
                        {
                            _agenteService.Edit(DeparaPessoaFisica(prospect, true));
                        }
                    }

                    //insertParametroFiscal
                    //InsertTelefones

                    //Para cada email
                    //  insiro um registro em pessoa_agente                

                    //Se possui dados bancarios
                    //inserre contasagnid

                    //Se possui pacto nupcial
                    // Insere campoAgenteEspecifico
                }

            }

            _repository.Commit();
            return 0; //Prospect convertido com sucesso
        }

        private EndAgente DeparaEnderecos(EnderecoProspect prospect, Agente agente, decimal sequencial)
        {
            EndAgente endAgente = new EndAgente();
            endAgente.Tab = 53;
            endAgente.Pad = agente.Pad;
            endAgente.IdAgente = agente.Id;
            endAgente.IdEndereco = sequencial;
            endAgente.SiglaPais = "BRA";
            endAgente.SiglaEstado = prospect.SiglaEstado;
            endAgente.TipoEndereco = prospect.TipoEnderecoProspect;
            endAgente.idMunicipio = prospect.CodigoMunicipio;
            endAgente.NomeLogradouro = prospect.Endereco;
            endAgente.Numero = prospect.Numero;
            endAgente.Bairro = prospect.Bairro;
            endAgente.Municipio = prospect.Municipio;
            endAgente.CEP = prospect.Cep;
            endAgente.Complemento = prospect.Complemento;
          

            return endAgente;
        }

        private Agente DeparaProspectAgente(Models.Prospect prospect)
        {
            Agente agente = new Agente();
            agente.Tab              = 53;
            agente.TipoOrganizacao  = "E";
            agente.NaturezaJuridica = 2992;
            agente.EnquadraIPI      = "S";
            agente.EnquadraICMS     = "S";
            agente.EnquadraISS      = "S";
            agente.FretePesoValor   = "P";
            agente.TabPai           = 53;
            agente.IPISimples       = "N";
            agente.ICMSSimples      = "N";
            agente.ISSSimples       = "N";
            agente.OpcaoSimples     = "N";
            agente.Fluxo            = "N";
            agente.Nivel            = 1;
            agente.Ordem            = "0009190";
            agente.UltimaAtualizacao= DateTime.Now;
            agente.ValidadeCadastroEmDias = 90;
            agente.Enquadramento    = "N";
            agente.CNAEId           = "9900-8/00";
            agente.ReterIR          = "N";
            agente.ReterINSS        = "N";
            agente.Inclusao         = 1;

            agente.Pad          = prospect.AgnPad;
            agente.Id           = _agenteService.getNewIdAgente();
            agente.NomeFantasia = prospect.Fantasia;
            agente.Nome = prospect.Nome;
            agente.TipoPessoa = prospect.TipoPessoa;
            agente.InscricaoMunicipal = prospect.InscricaoMunicipal;

            if (prospect.TipoPessoa == "J" && (prospect.Cnpj != null))
            {
                agente.CGC = prospect.Cnpj;
                agente.CGCStatus = Core.Validacoes.IsCNPJValido(prospect.Cnpj) ? "C" : "E";
            }
            else
            {
                agente.CGC = _agenteService.getNewIdAgente().ToString();
                agente.CGCStatus = "N";
            }

            agente.InscricaoEstadual = prospect.InscricaoEstadual;
            //agente.AtividadeEconomica = null
            agente.PadPai = prospect.AgnPad;
            agente.TabPai = agente.Tab;
            agente.IdPai = agente.Id;
            agente.Email = prospect.EMail;
            agente.Url = prospect.Site;
            agente.EnderecoPaisSigla = "BRA";

            return agente;
        }

        private AgenteCRM DeparaCRM(Models.Prospect prospect)
        {
            return new AgenteCRM()
            {
                Tab = prospect.AgnTab,
                Pad = prospect.AgnPad,
                IdAgente = prospect.AgnIn,
                IdIntegracao = prospect.IdIntegracao,
                Criacao = DateTime.Now,
                Alteracao = DateTime.Now
            };
        }

        private AgenteId DeparaId(Agente agente)
        {
            return new AgenteId()
            {
                Tab = agente.Tab,
                Pad = agente.Pad,
                IdAgente = agente.Id,
                Tau = "C",
                IdAlternativoAgente = agente.Id.ToString(),
                AtivadoSaldo = "S",
                ExcecaoFisscal = "S",
                DataCondicaoPagamento = "E",
                Status = "A"
            };
        }

        private PessoaFisica DeparaPessoaFisica(Models.Prospect prospect, bool isConjuge)
        {
            return new PessoaFisica()
            {
                AgenteTab = prospect.AgnTab,
                AgentePad = prospect.AgnPad,
                AgenteId = prospect.AgnIn,
                AgenteStatusCPF = Core.Validacoes.IsCPFValido(isConjuge ? prospect.CpfCje : prospect.Cpf) ? "C" : "E",
                CPF = isConjuge ? prospect.CpfCje : prospect.Cpf,
                Nascimento = isConjuge ? prospect.DataNascimentoCje : prospect.DataNascimento,
                LocalTrabalho = isConjuge ? prospect.LocalTrabalhoCje : prospect.LocalTrabalho,
                Cargo = isConjuge ? prospect.CargoCje.Nome :prospect.CargoProspect.Nome,
                EstadoCivil = prospect.EstadoCivil,
                RegimeCasamento = prospect.RegimeCasamento,
                Genero = isConjuge ? prospect.SexoCje : prospect.Sexo,
                Conjuge = prospect.NomeConjuge,
                TipoPessoa = isConjuge ? "C" : "P",
                AgentePaiTab = prospect.AgnTab,
                AgentePaiPad = prospect.AgnPad,
                AgentePaiId = prospect.AgnIn,
                AgentePaiTipo = isConjuge ? "C" : "P",             
                RGDataEmissao = isConjuge ? prospect.DataExpedicaoRgCje : prospect.DataExpedicaoRg,
                RGOrgaoEmissor = isConjuge ? prospect.OrgaoEmissorRgCje : prospect.OrgaoEmissorRg,
                CarteiraTrabalho = isConjuge ? prospect.CarteiraTrabalhoCje : prospect.CarteiraTrabalho,
                DataCasamento = prospect.DataCasamento,
                LocalNascimento = isConjuge ? prospect.LocalTrabalhoCje : prospect.LocalNascimento,
                NomePai = isConjuge ? prospect.NomePaiCje : prospect.NomePai,
                NomeMae = isConjuge ? prospect.NomeMaeCje : prospect.NomeMae,
                Nacionalidade = isConjuge ? prospect.NacionalidadeCje : prospect.Nacionalidade,
                DataAdmissao = isConjuge ? prospect.DataAdmissaoTrabalhoCje : prospect.DataAdmissaoTrabalho,
                TelefoneTrabalho = isConjuge ? prospect.TelefoneLocalTrabalhoCje : prospect.TelefoneLocalTrabalho,
                Salario = isConjuge ? prospect.SalarioTrabalhoCje : prospect.SalarioTrabalho,
                LocalTrabalhoConjuge = prospect.LocalTrabalhoCje,
                TelefoneTrabalhoConjuge = prospect.TelefoneLocalTrabalhoCje                
            };
        }

        private Cliente DeparaCliente(Agente agente)
        {
            return new Cliente()
            {
                AgenteTab = agente.Tab,
                AgentePad = agente.Pad,
                AgenteId = agente.Id,
                AgenteTau = "C",
                EmConcordate = "N",
                EmFalencia = "N",
                ProdutoControlaAplic = "N",
                CalculaFrete = "N",
                PredioPropio = "S"
            };
        }
    }   
}
