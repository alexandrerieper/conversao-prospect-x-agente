﻿using Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Models;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Services
{
    public class ProspectService : IProspectService
    {
        private IProspectRepository _repository;

        public ProspectService(IProspectRepository repository)
        {
            _repository = repository;
        }

        public bool Edit(Models.Prospect prospect)
        {
            return _repository.Edit(prospect);
        }

        public IEnumerable<Models.Prospect> Get()
        {
            return _repository.Get();
        }

        public Models.Prospect Get(decimal idProspect)
        {
            Models.Prospect prospect = _repository.Get(idProspect);
            prospect.CargoProspect.Nome = _repository.GetNomeCargo(prospect.CargoProspect.Id);
            prospect.CargoCje.Nome = _repository.GetNomeCargo(prospect.CargoCje.Id);
            return _repository.Get(idProspect);
        }

        public IEnumerable<EnderecoProspect> GetEnderecos(decimal idProspect)
        {
            return _repository.GetEnderecos(idProspect);
        }

        public IEnumerable<EnderecoProspect> GetEnderecosConjuge(decimal idProspect)
        {
            return _repository.GetEnderecosConjuge(idProspect);
        }

        public IEnumerable<TelefoneProspect> GetTelefones(decimal idProspect)
        {
            return _repository.GetTelefones(idProspect);
        }
    }
}
