﻿using Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Interfaces.Services;
using Mega.Erp.ComercialDBM.Prospects.Conversao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Services
{
    public class AgenteService : IAgenteService
    {
        private IAgenteRepository _repository;

        public AgenteService(IAgenteRepository repository)
        {
            _repository = repository;
        }

        public bool Delete(Agente agente)
        {
            throw new NotImplementedException();
        }

        public bool Delete(decimal idAgente)
        {
            return _repository.Delete(idAgente);
        }

        public bool DeleteAllEnderecos(Agente agente)
        {
            return _repository.DeleteAllEndereco(agente);
        }

        public bool Edit(Agente agente)
        {
            return _repository.Edit(agente);
        }

        public bool Edit(PessoaFisica pessoaFisica)
        {
            return _repository.Edit(pessoaFisica);
        }

        public AgenteId GetAgenteId(Agente agente)
        {
            return _repository.getAgenteId(agente);
        }

        public Agente GetById(decimal id)
        {
            return _repository.GetById(id);
        }

        public Agente GetByNomeCNPJ(string name, string cnpj)
        {
            return _repository.GetByNomeCNPJ(name, cnpj);
        }

        public Agente GetByNomeCPF(string name, string cpf)
        {
            return _repository.GetByNomeCPF(name, cpf);
        }

        public decimal getNewIdAgente()
        {
            return _repository.getNewIdAgente();
        }

        public bool Insert(Agente agente)
        {
            return _repository.Insert(agente);
        }

        public bool InsertCliente(Cliente cliente)
        {
            return _repository.InsertCliente(cliente);
        }

        public bool InsertCRM(AgenteCRM agenteCRM)
        {
            return _repository.InsertCRM(agenteCRM);
        }

        public bool InsertEndereco(EndAgente endAgente)
        {
            return _repository.Insert(endAgente);
        }

        public bool InsertId(AgenteId agenteId)
        {
            return _repository.InsertId(agenteId);
        }

        public bool InsertPessoaFisica(PessoaFisica pessoaFisica)
        {
            return _repository.InsertPessoaFisica(pessoaFisica);
        }
    }
}
