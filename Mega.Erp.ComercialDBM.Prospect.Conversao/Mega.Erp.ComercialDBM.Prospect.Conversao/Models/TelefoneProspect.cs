﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Models
{
    public class TelefoneProspect
    {
        public int Id { get; set; }
        public int IdProspect { get; set; }
        public int Ddd { get; set; }
        public string Numero { get; set; }
        public string Tipo { get; set; }
    }
}
