﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Models
{
    public class AgenteCampoEspecifico
    {
        public decimal AgenteTab; //AGN_TAB_IN_CODIGO
        public decimal AgentePad; //AGN_PAD_IN_CODIGO
        public decimal AgenteId; //AGN_IN_CODIGO
        public string PactoNupcial; //AGN_ST_PACTO_NUPCIAL
        public DateTime DataPacto;   //AGN_DT_PACTO_NUPCIAL
        public DateTime DataCasamento;   //AGN_DT_CASAMENTO
        public string Tabelionato; //AGN_ST_TABELIONATO
        public string Registro; //AGN_ST_REGISTRO
        public string Livro; //AGN_ST_LIVRO
        public string Folhas; //AGN_ST_FOLHAS
    }
}
