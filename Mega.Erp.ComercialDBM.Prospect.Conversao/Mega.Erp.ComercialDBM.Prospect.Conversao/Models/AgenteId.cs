﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Models
{
    public class AgenteId
    {
        public decimal Tab; //AGN_TAB_IN_CODIGO	
        public decimal Pad; //AGN_PAD_IN_CODIGO	
        public decimal IdAgente; //AGN_IN_CODIGO	
        public string Tau; // AGN_TAU_ST_CODIGO
        public decimal CategoriaTab; //CAT_TAB_IN_CODIGO
        public decimal CategoriaPad; // CAT_PAD_IN_CODIGO
        public string CategoriaIdentificador; //CAT_IDE_ST_CODIGO
        public string CategoriaIdReduzido; // CAT_IN_REDUZIDO
        public decimal ContaContabilTab; //PLA_TAB_IN_CODIGO
        public decimal ContaContabilPad; //PLA_PAD_IN_CODIGO
        public string ContaContabilCodigo; //PLA_IDE_ST_CODIGO
        public decimal ContaContabilIdReduzido;//PLA_IN_REDUZIDO 
        public string AtivadoSaldo; //AGN_BO_ATIVADOSALDO
        public string ExcecaoFisscal; //AGN_BO_EXCECAOFISCAL
        public string PrioridadePagamentoId; //PRIP_ST_CODIGO
        public decimal CondicaoPagamentoTab; //COND_TAB_IN_CODIGO
        public decimal CondicaoPagamentoPad; //COND_PAD_IN_CODIGO
        public string CondicaoPagamentoCodigo; //COND_ST_CODIGO
        public string DataCondicaoPagamento; //AGN_CH_DATABASE
        public string Status; // AGN_CH_STATUS CHAR(1)
        [System.Obsolete("Campo não utilizado.")]
        public decimal PagamentoGPS; //AGN_IN_PAGTOGPS	--Campo provavelmente depreciado
        public string IdAlternativoAgente; //AGN_ST_CODIGOALT
        public decimal IdEndereco; //ENA_IN_CODIGO	
        public decimal IdPessoaContato; //PAG_IN_CODIGO	
        public string SiglaPais; //PA_ST_SIGLA	
        public string SiglaEstado; //UF_ST_SIGLA	
        public string Sigla; //TPL_ST_SIGLA
        public string IdTipoEndereco; //TEA_ST_CODIGO
        public decimal IdMunicipio; //MUN_IN_CODIGO	
        public string Logradouro; //ENA_ST_LOGRADOURO
        public string Numero; //ENA_ST_NUMERO
        public string Bairro; //ENA_ST_BAIRRO
        public string Municipio; //ENA_ST_MUNICIPIO
        public string CEP; //ENA_ST_CEP	
        public string CaixaPostal; //ENA_ST_CXPOSTAL
        public string Complemento; //ENA_ST_COMPLEMENTO
        public string Referencia; //ENA_ST_REFERENCIA
        public string Telefone; //ENA_ST_TELEFONE
        public string Fax; //ENA_ST_FAX
        public string CNPJ; //ENA_ST_CNPJ	
        public string InscricaoEstadual; //ENA_ST_INSCRESTADUAL
        public string TipoInscricao; //ENA_CH_TIPOINSCRICAO	
        public string StatusCGC; //ENA_CH_STATUSCGC	
        public string StatusInscricaoEstadual; //ENA_CH_STATUSINSCRESTADUAL	
        public string CPF; //ENA_ST_CPF
        public string StatusCPF; //ENA_CH_STATUSCPF	
        public string EnderecoAgenteConjuge; //ENA_CH_ENDERECOPERTENCE	
    }
}
