﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Models
{
    public class Agente
    {
        public decimal Tab { set; get; } //AGN_TAB_IN_CODIGO
        public decimal Pad { set; get; } //AGN_PAD_IN_CODIGO
        public decimal Id { set; get; } //AGN_IN_CODIGO
        public string TipoOrganizacao { set; get; } //AGN_BO_CONSOLIDADOR
        public string NomeFantasia { set; get; } //AGN_ST_FANTASIA
        public string Nome { set; get; } //AGN_ST_NOME
        public string TipoPessoa { set; get; } //AGN_CH_TIPOPESSOAFJ
        public decimal NaturezaJuridica { set; get; } //AGN_IN_NATJURID
        public string InscricaoMunicipal { set; get; } //AGN_ST_INSCRMUNIC
        public string EstadoSigla { set; get; } //UF_ST_SIGLA
        public string EnderecoPaisSigla { set; get; } //PA_ST_SIGLA
        public decimal EnderecoMunicipioId { set; get; } //MUN_IN_CODIGO
        public string EnderecoMunicipioNome { set; get; } //AGN_ST_MUNICIPIO
        public string EnderecoLogradouroSigla { set; get; } //TPL_ST_SIGLA
        public string EnderecoLogradouroNome { set; get; } //AGN_ST_LOGRADOURO
        public string EnderecoNumeroPredio { set; get; } //AGN_ST_NUMERO
        public string EnderecoNomeBairro { set; get; } //AGN_ST_BAIRRO
        public string EnderecoCEP { set; get; } //AGN_ST_CEP
        public string EnderecoCaixaPostal { set; get; } //AGN_ST_CXPOSTAL
        public string EnderecoComplemento { set; get; } //AGN_ST_COMPLEMENTO
        public string EnderecoCEPCaixaPostal { set; get; } //AGN_ST_CEPCXPOSTAL
        public string EnderecoReferencia { set; get; } //AGN_ST_REFERENCIA
        public string EnquadraIPI { set; get; } //AGN_ST_ENQUADRAIPI
        public string EnquadraICMS { set; get; } //AGN_ST_ENQUADRAICMS
        public string EnquadraISS { set; get; } //AGN_ST_ENQUADRAISS
        public string CGC { set; get; } //AGN_ST_CGC
        public string CGCStatus { set; get; } //AGN_CH_STATUSCGC
        public string FretePesoValor { set; get; } //AGN_ST_FRETEPESOVALOR
        public string InscricaoEstadual { set; get; } //AGN_ST_INSCRESTADUAL
        public decimal TabPai { set; get; } //PAI_AGN_TAB_IN_CODIGO
        public string AtividadeEconomica { set; get; } //AGN_ST_ATIVECONOMICA
        public decimal PadPai { set; get; } //PAI_AGN_PAD_IN_CODIGO
        public decimal IdPai { set; get; } //PAI_AGN_IN_CODIGO
        public string Suframa { set; get; } //AGN_ST_SUFRAMA
        public string Reparticao { set; get; } //AGN_ST_REPARTICAO
        public string Email { set; get; } //AGN_ST_EMAIL
        public string IPISimples { set; get; } //AGN_BO_IPISIMPLES
        public string ICMSSimples { set; get; } //AGN_BO_ICMSSIMPLES
        public string ISSSimples { set; get; } //AGN_BO_ISSSIMPLES
        public string OpcaoSimples { set; get; } //AGN_BO_SIMPLES
        public decimal FormaPagamentoId { set; get; } //FORP_IN_CODIGO
        public string Fluxo { set; get; } //AGN_BO_FLUXO
        public decimal Nivel { set; get; } //AGN_IN_NIVEL
        public string Ordem { set; get; } //AGN_ST_ORDEM
        public DateTime UltimaAtualizacao { set; get; } //AGN_DT_ULTIMAATUCAD
        public decimal ValidadeCadastroEmDias { set; get; } //AGN_IN_VALIDADECAD
        public string CEI { set; get; } //AGN_ST_CEI
        public string TipoInscricao { set; get; } //AGN_CH_TIPOINSCRICAO
        public string Enquadramento { set; get; } //AGN_CH_ENQUADRAMENTO
        public string CNAEId { set; get; } //CNAE_ST_CODIGO
        public string ReterIR { set; get; } //AGN_BO_RETERIR
        public string ReterINSS { set; get; } //AGN_BO_RETERINSS
        public string Url { set; get; } //AGN_ST_URL
        public string Apelido { set; get; } //AGN_ST_APELIDO
        public string Senha { set; get; } //AGN_ST_SENHA
        public string StatusInscricaoEstadual { set; get; } //AGN_CH_STATUSINSCRESTADUAL
        public string Publico { set; get; } //AGN_BO_PUBLICO
        public string InscricaoProdutoRural { set; get; } //AGN_ST_INSCRPRODUTOR
        public decimal Inclusao { set; get; } //USU_IN_INCLUSAO
        public string TipoPessoaRural { set; get; } //AGN_CH_RURALTIPOPESSOAFJ
        public decimal ESocialId { set; get; } //TAB05_IN_CODIGO
        public string ESocialCAEPF { set; get; } //AGN_ST_NRCAEPF
        public string ESocialCNO { set; get; } //AGN_ST_NRCNO
        public string TipoDespesaAduaneira { set; get; } //AGN_CH_TIPODESPADUANEIRA
        public decimal TipoAtividadeEconomicaPF { set; get; } //AGN_IN_TIPOCAEPF
        public string NaturezaRetencaoPIS_CONFINS { set; get; } //AGN_CH_NATRETPISCOFINS
        public string PermiteRateioAFRMM { set; get; } //AGN_BO_RATEIOAFRMM
        public string TipoRateioAFRMM { set; get; } //AGN_CH_TIPOAFRMM
        public string PISMEI { set; get; } //AGN_ST_PISMEI
        public string CBOMEI { set; get; } //AGN_ST_CBOMEI
        public string NIF { set; get; } //AGN_ST_NIF
        public decimal FontePagadoraId { set; get; } //DFP_IN_CODIGO
        public string DispensadoNIF { set; get; } //AGN_BO_DISPENSADONIF   
    }
}
