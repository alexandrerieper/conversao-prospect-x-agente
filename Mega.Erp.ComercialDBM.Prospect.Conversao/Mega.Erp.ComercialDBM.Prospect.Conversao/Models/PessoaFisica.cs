﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Models
{
    public class PessoaFisica
    {
        public decimal AgenteTab; //AGN_TAB_IN_CODIGO
        public decimal AgentePad; //AGN_PAD_IN_CODIGO
        public decimal AgenteId; //AGN_IN_CODIGO
        public string AgenteStatusCPF; //AGN_CH_STATUSCPF	
        public string CPF; //AGN_ST_CPF	
        public string RG; //AGN_ST_RG	
        public DateTime Nascimento; //AGN_DT_NASCIMENTO
        public string LocalNascimento; //AGN_ST_LOCALNASCIM	
        public string EstadoNascimento; //AGN_ST_ESTADONASCIM	
        public string NomePai; //AGN_ST_NOMEPAI	
        public string NomeMae; //AGN_ST_NOMEMAE	
        public string LocalTrabalho; //AGN_ST_LOCALTRABALHO	
        public string TelefoneTrabalho; //AGN_ST_TELLOCTRAB	
        public DateTime DataAdmissao; //AGN_DT_ADMISSAO
        public string Cargo; //AGN_ST_CARGOPROFISS	
        public decimal Salario; //AGN_RE_SALARIO)
        public string CarteiraTrabalho; //AGN_ST_CARTTRABALHO	
        public string Nacionalidade; //AGN_ST_NACIONALIDADE	
        public string EstadoCivil; //AGN_CH_ESTCIVIL	
        public string Genero; //AGN_CH_SEXO	
        public string Conjuge; //AGN_ST_CONJUGE	
        public string LocalTrabalhoConjuge; //AGN_ST_LOCTRABCONJ	
        public string TelefoneTrabalhoConjuge; //AGN_ST_TELLOCTRABCONJ	
        public string TipoPessoa; //AGN_CH_TIPO	
        public decimal AgentePaiTab; //PAI_AGN_TAB_IN_CODIGO
        public decimal AgentePaiPad; //PAI_AGN_PAD_IN_CODIGO
        public decimal AgentePaiId; //PAI_AGN_IN_CODIGO
        public string AgentePaiTipo; //PAI_AGN_CH_TIPO	
        public string RegimeCasamento; //AGN_CH_REGIMECASAMENTO	
        public DateTime RGDataEmissao; //AGN_DT_EXPRG
        public string RGOrgaoEmissor; //AGN_ST_ORGEMISSORRG	
        public decimal ServicoId; //COS_IN_CODIGO
        public string Email; //AGN_ST_EMAIL	
        public string Nome; //AGN_ST_NOMEPESSOA	
        public string PIS; //AGN_ST_PIS	
        public string INSS; //AGN_ST_INSS	
        public string EspelhoCNH; //AGN_ST_ESPELHOCNH	
        public string CNH; //AGN_ST_REGISTROCNH	
        public DateTime CNHValidade; //AGN_DT_VALIDADECNH
        public string CNHCategoria; //AGN_ST_CATEGORIACNH	
        public DateTime CNHDataEmissao; //AGN_DT_EMISSAOCNH
        public DateTime CNHPrimeiraEmissao; //AGN_DT_PRIMHABCNH
        public string CNHModelo; //AGN_CH_MODELOCNH	
        public string CNHStatus; //AGN_CH_STATUSCNH	        
        public DateTime DataCasamento; //AGN_DT_CASAMENTO

    }
}
