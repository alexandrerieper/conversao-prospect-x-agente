﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Models
{
    public class ContaAgente
    {
        public decimal AgenteTab; //AGN_TAB_IN_CODIGO
        public decimal AgentePad; //AGN_PAD_IN_CODIGO
        public decimal AgenteId; //AGN_IN_CODIGO
        public string AgenteTau; //AGN_TAU_ST_CODIGO
        public decimal Banco; //CAID_BAN_IN_NUMERO
        public string Numero; //CAID_ST_CONTACORR
        public string Agencia; //CAIDAGE_ST_NUMERO
        public string ContaPreferencial; //CAID_BO_PREFERENCIAL
        public string TipoConta; //CAID_CH_TIPOCONTA
        public string ContaInativa; //CAID_BO_INATIVA
    }
}
