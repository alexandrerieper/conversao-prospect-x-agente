﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Models
{
    public class EndAgente
    {
        public decimal Tab; //AGN_TAB_IN_CODIGO
        public decimal Pad; //AGN_PAD_IN_CODIGO
        public decimal IdAgente; //AGN_IN_CODIGO
        public decimal IdEndereco; //ENA_IN_CODIGO
        public decimal ?IdPessoaContato; //PAG_IN_CODIGO
        public string SiglaPais; //PA_ST_SIGLA
        public string SiglaEstado; //UF_ST_SIGLA
        public string Sigla; //TPL_ST_SIGLA
        public string TipoEndereco; //TEA_ST_CODIGO
        public decimal idMunicipio; //MUN_IN_CODIGO
        public string NomeLogradouro; //ENA_ST_LOGRADOURO
        public string Numero; //ENA_ST_NUMERO
        public string Bairro; //ENA_ST_BAIRRO
        public string Municipio; //ENA_ST_MUNICIPIO
        public string CEP; //ENA_ST_CEP
        public string CaixaPosta; //ENA_ST_CXPOSTAL
        public string Complemento; //ENA_ST_COMPLEMENTO
        public string Referencia; //ENA_ST_REFERENCIA
        public string Telefone; //ENA_ST_TELEFONE
        public string Fax; //ENA_ST_FAX
    }
}
