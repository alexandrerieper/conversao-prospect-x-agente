﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Models
{
    public class ParametroFiscal
    {
        public decimal Tab; //AGN_TAB_IN_CODIGO
        public decimal Pad; //AGN_PAD_IN_CODIGO
        public decimal AgenteId; //AGN_IN_CODIGO
        public DateTime InicioVigencia; //AGN_DT_INIVIGENCIA
        public string EquadraIPI; //AGN_BO_ENQUADRAIPI
        public string EquadraICMS; //AGN_BO_ENQUADRAICMS
        public string EquadraISS; //AGN_BO_ENQUADRAISS
        public string ReterIR; //AGN_BO_RETERIR
        public string ReterINSS; //AGN_BO_RETERINSS
        public string OpcaoSimples; //AGN_BO_SIMPLES
        public string SimplesIPI; //AGN_BO_IPISIMPLES
        public string SimplesICMSS; //AGN_BO_ICMSSIMPLES
        public string SimplesISS; //AGN_BO_ISSSIMPLES
        public string Observacao; //AGN_ST_OBS
        public string EscriturarMovFiscal; //AGN_BO_ESCRITURAR
    }
}
