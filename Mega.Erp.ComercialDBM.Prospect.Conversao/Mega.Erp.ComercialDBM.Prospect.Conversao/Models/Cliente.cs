﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Models
{
    public class Cliente
    {
        public decimal AgenteTab; //AGN_TAB_IN_CODIGO
        public decimal AgentePad; //AGN_PAD_IN_CODIGO
        public decimal AgenteId; //AGN_IN_CODIGO
        public string AgenteTau; //AGN_TAU_ST_CODIGO
        public string EmConcordate; //CLI_CH_CONCORDATARIO
        public string EmFalencia; //CLI_CH_PROCFALENCIA
        public string ProdutoControlaAplic; //CLI_BO_APLICACAOPRODUTO
        public string CalculaFrete; //CLI_BO_FRETEAUTOMATICO
        public string PredioPropio; //CLI_BO_PREDIOPROPRIO
    }
}
