﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Models
{
    public class Cargo
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
