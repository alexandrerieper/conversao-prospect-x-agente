﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Models
{
    public class EnderecoProspect
    {
        public int Id { get; set; }
        public int IdProspect { get; set; }
        public string SiglaPais { get; set; }
        public string SiglaEstado { get; set; }
        public int CodigoMunicipio { get; set; }
        public string SiglaRua { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Numero { get; set; }
        public string Municipio { get; set; }
        public string Cep { get; set; }
        public string Complemento { get; set; }
        public string TipoEnderecoAgente { get; set; }
        public string TipoEnderecoProspect { get; set; }     

    }
}
