﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Models
{
    public class PessoaAgente
    {
        public decimal Tab; //AGN_TAB_IN_CODIGO
        public decimal Pad; //AGN_PAD_IN_CODIGO
        public decimal AgenteId; //AGN_IN_CODIGO
        public decimal Id; //PAG_IN_CODIGO
        public string Nome; //PAG_ST_NOME
        public string Email; //PAG_ST_EMAIL
        public string Tipo; //PAG_CH_TIPO
        public decimal RelacionamentoTipo; //TPR_IN_CODIGO
        public string Genero; //PAG_CH_SEXO
        public string EstadoCivil; //PAG_CH_ESTADOCIVIL
        public string ResponsavelFiscal; //PAG_BO_RESPFISCAL
        public string ResponsavelFinanceiro; //PAG_BO_RESPFINANC
    }
}
