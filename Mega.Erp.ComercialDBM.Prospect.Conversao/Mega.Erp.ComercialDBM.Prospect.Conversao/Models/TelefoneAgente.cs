﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Models
{
    public class TelefoneAgente
    {
        public decimal Tab; //AGN_TAB_IN_CODIGO
        public decimal Pad; //AGN_PAD_IN_CODIGO
        public decimal IdAgente; //AGN_IN_CODIGO
        public string Numero; //TEA_ST_TELEFONE
        public string Tipo; //TEA_ST_TIPO
    }
}
