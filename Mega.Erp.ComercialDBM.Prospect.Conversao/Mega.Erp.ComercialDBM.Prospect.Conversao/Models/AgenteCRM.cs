﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Models
{
    public class AgenteCRM
    {
        public decimal Tab; //AGN_TAB_IN_CODIGO
        public decimal Pad; //AGN_PAD_IN_CODIGO
        public decimal IdAgente; //AGN_IN_CODIGO
        public string IdIntegracao; //ID_INTEGRACAO
        public DateTime Criacao; //DATA_CRIACAO
        public DateTime Alteracao; //DATA_ALTERACAO
        public DateTime EnvioCRM; //DATA_ENVIO_CRM

    }
}
