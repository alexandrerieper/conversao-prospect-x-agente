﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.ComercialDBM.Prospects.Conversao.Models
{
    public class Prospect
    {
        // Dados Prospect
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Fantasia { get; set; }
        public string TipoPessoa { get; set; }
        public string Sexo { get; set; }
        public string EstadoCivil { get; set; }
        public int NumeroFilhos { get; set; }
        public string EMail { get; set; }
        public string Site { get; set; }
        public string Rg { get; set; }
        public string OrgaoEmissorRg { get; set; }
        public DateTime DataExpedicaoRg { get; set; }
        public string Cpf { get; set; }
        public string StatusCpf { get; set; }
        public string Cnpj { get; set; }
        public string InscricaoEstadual { get; set; }
        public string InscricaoMunicipal { get; set; }
        public string Cei { get; set; }
        public DateTime DataNascimento { get; set; }
        public string LocalNascimento { get; set; }
        public string Nacionalidade { get; set; }
        public string NomePai { get; set; }
        public string NomeMae { get; set; }
        public string EmpresaTrabalho { get; set; }
        public DateTime DataAdmissaoTrabalho { get; set; }
        public string CarteiraTrabalho { get; set; }
        public string LocalTrabalho { get; set; }
        public string TelefoneLocalTrabalho { get; set; }
        public decimal SalarioTrabalho { get; set; }
        public Cargo CargoProspect { get; set; }

        // Dados Casamento
        public string RegimeCasamento { get; set; }
        public DateTime DataCasamento { get; set; }
        public string PactoNupcial { get; set; }
        public DateTime DataPactoNupcial { get; set; }
        public string TabelionatoCasamento { get; set; }
        public string RegistroCasamento { get; set; }
        public string LivroCasamento { get; set; }
        public string FolhaCasamento { get; set; }
        
        // Cônjuge
        public string NomeConjuge { get; set; }
        public string SexoCje { get; set; }
        public string EMailCje { get; set; }
        public string RgCje { get; set; }
        public string OrgaoEmissorRgCje { get; set; }
        public DateTime DataExpedicaoRgCje { get; set; }
        public string CpfCje { get; set; }
        public string StatusCpfCje { get; set; }
        public DateTime DataNascimentoCje { get; set; }
        public string LocalNascimentoCje { get; set; }
        public string NacionalidadeCje { get; set; }
        public string NomePaiCje { get; set; }
        public string NomeMaeCje { get; set; }
        public string EmpresaTrabalhoCje { get; set; }
        public DateTime DataAdmissaoTrabalhoCje { get; set; }
        public string CarteiraTrabalhoCje { get; set; }
        public string LocalTrabalhoCje { get; set; }
        public string TelefoneLocalTrabalhoCje { get; set; }
        public decimal SalarioTrabalhoCje { get; set; }
        public Cargo CargoCje { get; set; }

        // Chave prospect -> Agente
        public decimal AgnTab { get; set; }
        public decimal AgnPad { get; set; }
        public decimal AgnIn { get; set; }
        
        // Dados bancários
        public decimal NumeroBanco { get; set; }
        public string AgenciaBanco { get; set; }
        public string ContaBanco { get; set; }
        public string IdIntegracao { get; set; }
    }
}
